import { Component, ViewChild } from '@angular/core';
import { MatPaginator, MatSort } from '@angular/material';
import { ListingDataSource } from './listing-datasource';
import { ListingService } from './listing.service';
import { filter } from 'rxjs/operators';
import { NavigationEnd, Router, ActivatedRoute } from '@angular/router';
import { LIST } from '../../constants/storage';

@Component({
  selector: 'app-listing',
  templateUrl: './listing.component.html',
  styleUrls: ['./listing.component.css'],
})
export class ListingComponent {
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  dataSource: ListingDataSource;

  displayedColumns = ['name', 'uuid'];
  model: string;
  search: string = '';

  constructor(
    private listingService: ListingService,
    private router: Router,
    private activatedRoute: ActivatedRoute,
  ) {
    this.model = this.activatedRoute.snapshot.params.model;
  }

  ngOnInit() {
    this.router.events
      .pipe(filter(route => route instanceof NavigationEnd))
      .subscribe((route: NavigationEnd) => {
        // https://[url]/list/[model]
        this.model = route.url.split('/')[2];
        if (route.url.split('/')[1] === LIST) {
          this.updateList();
        }
      });
    this.updateList();
  }

  getUpdate(event) {
    this.dataSource.loadItems(
      this.search,
      this.sort.direction,
      event.pageIndex,
      event.pageSize,
    );
  }

  setFilter() {
    this.dataSource.loadItems(
      this.search,
      this.sort.direction,
      this.paginator.pageIndex,
      this.paginator.pageSize,
    );
  }

  snakeToTitleCase(string: string) {
    if (!string) return;

    return string
      .split('_')
      .map(word => word.charAt(0).toUpperCase() + word.slice(1))
      .join(' ');
  }

  updateList() {
    this.dataSource = new ListingDataSource(this.model, this.listingService);
    this.dataSource.loadItems();
  }
}
