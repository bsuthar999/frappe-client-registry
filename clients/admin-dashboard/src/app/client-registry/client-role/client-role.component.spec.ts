import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { ClientRoleComponent } from './client-role.component';
import { MaterialModule } from '../../shared-imports/material/material.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { HttpErrorHandler } from '../../common/services/http-error-handler/http-error-handler.service';
import { MessageService } from '../../common/services/message/message.service';
import { oauthServiceStub } from '../../common/testing-helpers';
import { OAuthService } from 'angular-oauth2-oidc';
import { RouterTestingModule } from '@angular/router/testing';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ClientRoleService } from './client-role.service';
import { RoleService } from '../role/role.service';
import { MatSnackBar, MatChipsModule } from '@angular/material';
import { ListingService } from '../../../app/shared-ui/listing/listing.service';
import { of } from 'rxjs';

describe('ClientRoleComponent', () => {
  let component: ClientRoleComponent;
  let fixture: ComponentFixture<ClientRoleComponent>;
  const API_RESPONSE_MOCK = { docs: [{ mock: 'mock' }] };
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        MaterialModule,
        FormsModule,
        ReactiveFormsModule,
        HttpClientTestingModule,
        RouterTestingModule,
        BrowserAnimationsModule,
        MatChipsModule,
      ],
      providers: [
        HttpErrorHandler,
        MessageService,
        {
          provide: OAuthService,
          useValue: oauthServiceStub,
        },
        {
          provide: ClientRoleService,
          useValue: {},
        },
        {
          provide: RoleService,
          useValue: {
            getRoles: (...args) => of(API_RESPONSE_MOCK),
          },
        },
        {
          provide: MatSnackBar,
          useValue: {},
        },
        {
          provide: ListingService,
          useValue: {
            findModels: (...args) => of(API_RESPONSE_MOCK),
          },
        },
      ],
      declarations: [ClientRoleComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClientRoleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
