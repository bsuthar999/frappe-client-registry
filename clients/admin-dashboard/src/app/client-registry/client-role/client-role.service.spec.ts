import { TestBed } from '@angular/core/testing';
import { ClientRoleService } from './client-role.service';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { HttpErrorHandler } from '../../common/services/http-error-handler/http-error-handler.service';
import { HttpClient } from '@angular/common/http';
import { OAuthService } from 'angular-oauth2-oidc';

describe('ClientRoleService', () => {
  beforeEach(() =>
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [
        {
          provide: HttpErrorHandler,
          useValue: {
            handleError<T>(...args) {},
            createHandleError(...args) {},
          },
        },
        {
          provide: OAuthService,
          useValue: {
            getAccessToken: (...args) => 'mock_token',
          },
        },
        {
          provide: HttpClient,
          useValue: {},
        },
      ],
    }),
  );

  it('should be created', () => {
    const service: ClientRoleService = TestBed.get(ClientRoleService);
    expect(service).toBeTruthy();
  });
});
