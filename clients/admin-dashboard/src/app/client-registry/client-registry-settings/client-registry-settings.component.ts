import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { MatSnackBar } from '@angular/material';
import { ClientRegistrySettingsService } from './client-registry-settings.service';
import {
  UPDATE_SUCCESSFUL,
  CLOSE,
  SETUP_COMPLETED_SUCCESSFUL,
} from '../../constants/messages';
import { DURATION } from '../../constants/common';

@Component({
  selector: 'app-client-registry-settings',
  templateUrl: './client-registry-settings.component.html',
  styleUrls: ['./client-registry-settings.component.css'],
})
export class ClientRegistrySettingsComponent implements OnInit {
  appURL: string;
  clientId: string;
  clientSecret: string;
  hideClientSecret = true;
  frappeSystemManagerUuid: string;
  clientTokenUuid: string;

  settingsForm = new FormGroup({
    appURL: new FormControl(this.appURL),
    clientId: new FormControl(this.clientId),
    clientSecret: new FormControl(this.clientSecret),
    frappeSystemManagerUuid: new FormControl(this.frappeSystemManagerUuid),
    clientTokenUuid: new FormControl(this.clientTokenUuid),
  });

  constructor(
    private settingsService: ClientRegistrySettingsService,
    private snackBar: MatSnackBar,
  ) {}

  ngOnInit() {
    this.settingsService.getSettings().subscribe({
      next: (response: any) => {
        this.appURL = response.appUrl;
        this.frappeSystemManagerUuid = response.frappeSystemManagerUuid;
        this.clientTokenUuid = response.clientTokenUuid;
        this.populateForm(response);
      },
      error: error => {},
    });
  }

  populateForm(response) {
    this.settingsForm.controls.appURL.setValue(response.appURL);
    this.settingsForm.controls.clientId.setValue(response.clientId);
    this.settingsForm.controls.clientSecret.setValue(response.clientSecret);
  }

  updateSettings() {
    this.settingsService
      .updateSettings(
        this.settingsForm.controls.appURL.value,
        this.settingsForm.controls.clientId.value,
        this.settingsForm.controls.clientSecret.value,
      )
      .subscribe({
        next: response => {
          this.snackBar.open(UPDATE_SUCCESSFUL, CLOSE, { duration: DURATION });
        },
        error: error =>
          this.snackBar.open(error.error.messages, CLOSE, {
            duration: DURATION,
          }),
      });
  }

  setupPermission() {
    this.settingsService.setupPermission().subscribe({
      next: success => {
        this.snackBar.open(SETUP_COMPLETED_SUCCESSFUL, CLOSE, {
          duration: DURATION,
        });
        location.reload();
      },

      error: error => {
        this.snackBar.open(error.error.messages, CLOSE, { duration: DURATION });
      },
    });
  }
}
