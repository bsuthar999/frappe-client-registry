import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { Router } from '@angular/router';
import { MatSnackBar } from '@angular/material';
import { ClientService } from '../client.service';
import { CREATE_SUCCESSFUL, CLOSE } from '../../../constants/messages';
import { DURATION } from '../../../constants/common';
import { Location } from '@angular/common';

@Component({
  selector: 'app-frappe-instance',
  templateUrl: './frappe-instance.component.html',
  styleUrls: ['./frappe-instance.component.css'],
})
export class FrappeInstanceComponent implements OnInit {
  uuid;
  hideBloomstackBotPassword = true;

  FrappeInstanceForm = new FormGroup({
    frappeClientUuid: new FormControl(''),
    clientUuid: new FormControl(this.uuid),
    frappeName: new FormControl(''),
    bloomstackBotPassword: new FormControl(''),
  });

  constructor(
    private readonly clientService: ClientService,
    private router: Router,
    private snackBar: MatSnackBar,
    private location: Location,
  ) {
    this.getClientUuid();
  }

  ngOnInit() {}

  addFrappeInstance() {
    this.clientService
      .addFrappetance(
        this.FrappeInstanceForm.controls.frappeClientUuid.value,
        this.FrappeInstanceForm.controls.clientUuid.value,
        this.FrappeInstanceForm.controls.frappeName.value,
        this.FrappeInstanceForm.controls.bloomstackBotPassword.value,
      )
      .subscribe({
        next: success => {
          this.snackBar.open(CREATE_SUCCESSFUL, CLOSE, { duration: DURATION });
          this.location.back();
        },
        error: err => {
          this.snackBar.open(err.error.message, CLOSE, { duration: 1000 });
        },
      });
  }

  getClientUuid() {
    this.uuid = this.clientService.getUuid();
    this.FrappeInstanceForm.controls.clientUuid.setValue(this.uuid);
  }
}
