import { Component, OnInit } from '@angular/core';
import { ClientService } from './client.service';
import { ActivatedRoute, Router } from '@angular/router';
import { MatSnackBar } from '@angular/material';
import { FormGroup, FormControl } from '@angular/forms';
import {
  CREATE_SUCCESSFUL,
  UPDATE_SUCCESSFUL,
  CLOSE,
  CREATE_ERROR,
  UPDATE_ERROR,
  CLIENT_HAS_NO_FRAPPE_INSTANCE,
  FRAPPE_KEY_ADDED_SUCCESSFULLY,
  FRAPPE_KEY_UPDATED_SUCCESSFULLY,
} from '../../constants/messages';
import { NEW_ID, DURATION } from '../../constants/common';
import { CLIENT_LIST_ROUTE } from '../../constants/url-paths';

@Component({
  selector: 'app-client',
  templateUrl: './client.component.html',
  styleUrls: ['./client.component.css'],
})
export class ClientComponent implements OnInit {
  columnsToDisplay: string[] = ['UUID', 'NAME'];
  frappeInstanceData;

  frappeInstances: string[] = [];
  uuid: string;
  name: string;

  clientForm = new FormGroup({
    clientName: new FormControl(this.name),
  });

  constructor(
    private readonly clientService: ClientService,
    private route: ActivatedRoute,
    private router: Router,
    private snackBar: MatSnackBar,
  ) {
    this.uuid = this.route.snapshot.params.uuid;
  }

  ngOnInit() {
    if (this.uuid !== NEW_ID) {
      this.clientService.getClient(this.uuid).subscribe({
        next: response => {
          this.name = response[0].name;
          this.clientForm.controls.clientName.setValue(response[0].name);
          response[0].frappeInstance
            ? this.getFrappeInstance()
            : this.snackBar.open(CLIENT_HAS_NO_FRAPPE_INSTANCE, CLOSE, {
                duration: DURATION,
              });
        },
        error: error =>
          this.snackBar.open(error.error.messages, CLOSE, {
            duration: DURATION,
          }),
      });
    }
  }

  createClient() {
    this.clientService
      .createClient(this.clientForm.controls.clientName.value)
      .subscribe({
        next: success => {
          this.snackBar.open(CREATE_SUCCESSFUL, CLOSE, { duration: DURATION });
          this.router.navigateByUrl(CLIENT_LIST_ROUTE);
        },
        error: error =>
          this.snackBar.open(CREATE_ERROR, CLOSE, { duration: DURATION }),
      });
  }

  updateClient() {
    this.clientService
      .updateClient(this.uuid, this.clientForm.controls.clientName.value)
      .subscribe({
        next: success => {
          this.snackBar.open(UPDATE_SUCCESSFUL, CLOSE, { duration: DURATION });
          this.router.navigateByUrl(CLIENT_LIST_ROUTE);
        },
        error: error =>
          this.snackBar.open(UPDATE_ERROR, CLOSE, { duration: DURATION }),
      });
  }
  getFrappeInstance() {
    this.clientService.getClient(this.uuid).subscribe({
      next: success => {
        success[0].frappeInstance.map(frappeInstance => {
          return this.frappeInstances.push(frappeInstance);
        });
      },
    });

    this.frappeInstanceData = this.frappeInstances;
  }

  addFrappeKey(clientUuid: string, frappeClientUuid: string) {
    this.clientService.addFrappeKey(clientUuid, frappeClientUuid).subscribe({
      next: success => {
        this.snackBar.open(FRAPPE_KEY_ADDED_SUCCESSFULLY, CLOSE, {
          duration: DURATION,
        });
      },
      error: err => {
        this.snackBar.open(err.error.message, CLOSE, { duration: DURATION });
      },
    });
  }

  updateFrappeKey(clientUuid: string, frappeClientUuid: string) {
    this.clientService.updateFrappeKey(clientUuid, frappeClientUuid).subscribe({
      next: success => {
        this.snackBar.open(FRAPPE_KEY_UPDATED_SUCCESSFULLY, CLOSE, {
          duration: DURATION,
        });
      },
      error: err => {
        this.snackBar.open(err.error.message, CLOSE, { duration: DURATION });
      },
    });
  }
}
