import { Injectable } from '@angular/core';
import {
  HandleError,
  HttpErrorHandler,
} from '../../common/services/http-error-handler/http-error-handler.service';
import { Observable } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { CANNOT_FETCH_CLIENT } from '../../constants/messages';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { OAuthService } from 'angular-oauth2-oidc';
import {
  ADD_FRAPPE_INSTANCE,
  UPDATE_CLIENT,
  GET_CLIENT,
  ADD_FRAPPE_API_KEY,
  UPDATE_FRAPPE_API_KEY,
} from '../../../app/constants/url-paths';

@Injectable({
  providedIn: 'root',
})
export class ClientService {
  clientUuid;
  private handlerror: HandleError;
  constructor(
    private readonly http: HttpClient,
    httpErrorHandler: HttpErrorHandler,
    private oauthService: OAuthService,
  ) {
    this.handlerror = httpErrorHandler.createHandleError('ClientService');
  }

  createClient(clientName: string) {
    const url = `api/client/v1/create`;
    const clientData = {
      name: clientName,
    };
    return this.http.post(url, clientData, {
      headers: this.getHeaders(),
    });
  }

  addFrappetance(
    frappeClientUuid: string,
    clientUuid: string,
    name: string,
    bloomstackBotPassword: string,
  ) {
    const frappeInstanceData = {
      frappeClientUuid,
      clientUuid,
      name,
      bloomstackBotPassword,
    };

    return this.http.post(ADD_FRAPPE_INSTANCE, frappeInstanceData, {
      headers: this.getHeaders(),
    });
  }

  getClient(client: string): Observable<any> {
    this.clientUuid = client;
    const url = GET_CLIENT + `/${client}`;
    return this.http
      .get<string>(url, { headers: this.getHeaders() })
      .pipe(
        catchError(
          this.handlerror('getClient', { message: CANNOT_FETCH_CLIENT }),
        ),
      );
  }

  updateClient(uuid: string, clientName: string) {
    const clientData = {
      name: clientName,
      uuid,
    };
    return this.http.post(UPDATE_CLIENT, clientData, {
      headers: this.getHeaders(),
    });
  }

  getUuid() {
    return this.clientUuid;
  }

  getHeaders() {
    return new HttpHeaders({
      Authorization: 'Bearer ' + this.oauthService.getAccessToken(),
    });
  }

  addFrappeKey(clientUuid: string, frappeClientUuid: string) {
    const frappeData = {
      clientUuid,
      frappeClientUuid,
    };
    return this.http.post(ADD_FRAPPE_API_KEY, frappeData, {
      headers: this.getHeaders(),
    });
  }

  updateFrappeKey(clientUuid: string, frappeClientUuid: string) {
    const frappeData = {
      clientUuid,
      frappeClientUuid,
    };
    return this.http.post(UPDATE_FRAPPE_API_KEY, frappeData, {
      headers: this.getHeaders(),
    });
  }
}
