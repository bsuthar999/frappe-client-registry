import { Injectable } from '@angular/core';
import {
  HandleError,
  HttpErrorHandler,
} from '../../common/services/http-error-handler/http-error-handler.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { CANNOT_FETCH_CLIENT } from '../../constants/messages';
import { OAuthService } from 'angular-oauth2-oidc';

@Injectable({
  providedIn: 'root',
})
export class UserService {
  private handleError: HandleError;

  constructor(
    private readonly http: HttpClient,
    httpErrorHandler: HttpErrorHandler,
    private readonly oAuthService: OAuthService,
  ) {
    this.handleError = httpErrorHandler.createHandleError('UserService');
  }

  getUser(userUUID: string): Observable<any> {
    const url = `api/user/v1/get/${userUUID}`;
    return this.http
      .get<string>(url, { headers: this.getHeaders() })
      .pipe(
        catchError(
          this.handleError('getUser', { message: CANNOT_FETCH_CLIENT }),
        ),
      );
  }

  verifyUser(userURL: string) {
    const url = `${userURL}/info`;
    return this.http.get<string>(url);
  }

  getHeaders() {
    return new HttpHeaders({
      Authorization: 'Bearer ' + this.oAuthService.getAccessToken(),
    });
  }
}
