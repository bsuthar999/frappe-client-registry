import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { OAuthModule } from 'angular-oauth2-oidc';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { SharedUIModule } from './shared-ui/shared-ui.module';
import { AppService } from './app.service';
import { ClientRegistryUIModule } from './client-registry/client-registry-ui.module';

@NgModule({
  declarations: [AppComponent],
  imports: [
    BrowserModule,
    AppRoutingModule,
    OAuthModule.forRoot(),
    SharedUIModule,
    ClientRegistryUIModule,
  ],
  providers: [AppService],
  bootstrap: [AppComponent],
})
export class AppModule {}
