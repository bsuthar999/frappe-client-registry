import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ListingComponent } from './shared-ui/listing/listing.component';
import { AuthGuard } from './common/guards/auth-guard/auth.guard.service';
import { HomeComponent } from './shared-ui/home/home.component';
import { DashboardComponent } from './shared-ui/dashboard/dashboard.component';
import { ClientRegistrySettingsComponent } from './client-registry/client-registry-settings/client-registry-settings.component';
import { RoleComponent } from './client-registry/role/role.component';
import { UserComponent } from './client-registry/user/user.component';
import { ClientComponent } from './client-registry/client/client.component';
import { ClientRoleComponent } from './client-registry/client-role/client-role.component';
import { FrappeInstanceComponent } from './client-registry/client/frappe-instance/frappe-instance.component';

const routes: Routes = [
  { path: 'home', component: HomeComponent },
  { path: 'dashboard', component: DashboardComponent },
  {
    path: 'settings',
    component: ClientRegistrySettingsComponent,
    canActivate: [AuthGuard],
  },
  {
    path: 'role/:uuid',
    component: RoleComponent,
    canActivate: [AuthGuard],
  },
  {
    path: 'client/:uuid',
    component: ClientComponent,
    canActivate: [AuthGuard],
  },
  {
    path: 'client-role/:uuid',
    component: ClientRoleComponent,
    canActivate: [AuthGuard],
  },
  {
    path: 'user/:uuid',
    component: UserComponent,
    canActivate: [AuthGuard],
  },
  {
    path: 'list/:model',
    component: ListingComponent,
    canActivate: [AuthGuard],
  },
  {
    path: 'add-frappe-instance',
    component: FrappeInstanceComponent,
    canActivate: [AuthGuard],
  },

  { path: '', redirectTo: 'home', pathMatch: 'full' },
  { path: '**', redirectTo: 'home' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
