import { Module, HttpModule } from '@nestjs/common';
import { EventStoreSagas } from './sagas';
import { EventStoreCommandHandlers } from './commands';
import { EventStoreAggregates } from './aggregates';
import { CqrsModule } from '@nestjs/cqrs';
import { EventStoreController } from './controllers/event-store/event-store.controller';

@Module({
  imports: [HttpModule, CqrsModule],
  controllers: [EventStoreController],
  providers: [
    ...EventStoreAggregates,
    ...EventStoreSagas,
    ...EventStoreCommandHandlers,
  ],
})
export class EventStoreModule {}
