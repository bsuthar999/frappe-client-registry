import { Controller, UseInterceptors, HttpException } from '@nestjs/common';
import { EventPattern } from '@nestjs/microservices';
import { EventStoreInterface } from './event-store.dto';
import { CommandBus } from '@nestjs/cqrs';
import { AddNewUserCommand } from '../../../user/commands/add-new-user/add-new-user.command';
import { EventStoreContext } from '../../microservice/event-store.context';
import { RavenInterceptor } from 'nest-raven';

export const SOCIAL_LOGIN_USER_SIGNED_UP_EVENT = 'SocialLoginUserSignedUpEvent';
export const RETRY = 3;
@Controller()
@UseInterceptors(
  new RavenInterceptor({
    filters: [
      {
        type: HttpException,
        filter: (exception: HttpException) => 499 < exception.getStatus(),
      },
    ],
  }),
)
export class EventStoreController {
  constructor(private readonly commandBus: CommandBus) {}

  @EventPattern(SOCIAL_LOGIN_USER_SIGNED_UP_EVENT)
  resolveEvent(payload: EventStoreInterface, ctx: EventStoreContext) {
    this.commandBus
      .execute(new AddNewUserCommand(payload.data, RETRY, payload.eventNumber))
      .then(success => {})
      .catch(err => {});
    return;
  }
}
