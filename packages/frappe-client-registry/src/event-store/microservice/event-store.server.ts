import {
  CustomTransportStrategy,
  Server,
  ReadPacket,
} from '@nestjs/microservices';
import { TCPClient } from 'geteventstore-promise';
import { Observable } from 'rxjs';
import { EventStoreContext } from './event-store.context';

export class EventStoreServer extends Server
  implements CustomTransportStrategy {
  server: TCPClient;

  constructor(
    private readonly hostname: string,
    private readonly username: string,
    private readonly password: string,
    private readonly stream: string,
  ) {
    super();
  }

  async listen(callback: () => void) {
    this.init();
    await this.subscribeEvents();
    callback && callback();
  }

  async close() {
    if (this.server) {
      await this.server.close();
    }
  }

  init() {
    if (this.hostname && this.stream && this.username && this.password) {
      this.server = new TCPClient({
        hostname: this.hostname,
        port: 1113,
        credentials: { username: this.username, password: this.password },
      });
    }
  }

  async subscribeEvents() {
    if (this.server) {
      await this.server.subscribeToStream(
        this.stream,
        this.processEvent.bind(this),
      );
    }
  }

  async processEvent(subscription, payload) {
    const packet: ReadPacket = {
      data: payload,
      pattern: payload.eventType,
    };

    await this.handleEvent(packet.pattern, packet);
    await this.handleMessage(packet);
  }

  async handleEvent(pattern, packet) {
    const eventStoreCtx = new EventStoreContext([this.stream]);

    // Mute Errors if EventPattern not found
    this.logger.error = (...args) => {};
    super.handleEvent(pattern, packet, eventStoreCtx);
  }

  async handleMessage(packet: ReadPacket) {
    const pattern = JSON.stringify({ cmd: packet.pattern });

    const handler = this.getHandlerByPattern(pattern);
    if (!handler || handler.isEventHandler) {
      return;
    }

    const response$ = this.transformToObservable(
      await handler(packet.data),
    ) as Observable<any>;

    response$ && this.send(response$, data => {});
  }
}
