import { Column, Entity, BaseEntity, ObjectID, ObjectIdColumn } from 'typeorm';
import * as uuidv4 from 'uuid/v4';
import { SERVICE } from '../../../constants/app-strings';
import { IsUrl } from 'class-validator';

@Entity()
export class ServerSettings extends BaseEntity {
  @ObjectIdColumn()
  _id: ObjectID;

  @Column()
  uuid: string;

  @Column()
  appURL: string;

  @Column()
  authServerURL: string;

  @Column()
  clientId: string;

  @Column()
  clientSecret: string;

  @Column()
  clientUuid: string;

  @Column()
  profileURL: string;

  @Column()
  frappeSystemManagerUuid: string;

  @Column()
  clientTokenUuid: string;

  @Column()
  tokenURL: string;

  @Column()
  introspectionURL: string;

  @Column()
  authorizationURL: string;

  @Column()
  callbackURLs: string[];

  @Column()
  revocationURL: string;

  @Column()
  service: string = SERVICE;

  @Column()
  cloudStorageSettings: string;

  @Column()
  @IsUrl()
  infrastructureURL: string;

  @Column()
  @IsUrl()
  identityProviderURL: string;

  @Column()
  @IsUrl()
  communicationServerURL: string;

  @Column()
  @IsUrl()
  decafServerUrl: string;

  constructor() {
    super();
    if (!this.uuid) this.uuid = uuidv4();
  }
}
