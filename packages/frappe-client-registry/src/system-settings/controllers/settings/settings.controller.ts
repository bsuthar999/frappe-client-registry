import {
  Controller,
  Get,
  UsePipes,
  ValidationPipe,
  Body,
  Post,
  UseGuards,
  Req,
  UseInterceptors,
  HttpException,
} from '@nestjs/common';
import { CommandBus } from '@nestjs/cqrs';
import { from } from 'rxjs';
import { switchMap } from 'rxjs/operators';
import { ADMINISTRATOR } from '../../../constants/app-strings';
import { TokenGuard } from '../../../auth/guards/token.guard';
import { RoleGuard } from '../../../auth/guards/role.guard';
import { Roles } from '../../../auth/decorators/roles.decorator';
import { SettingsService } from '../../aggregates/settings/settings.service';
import { RegisterServiceCommand } from '../../commands/register-service/register-service.command';
import { UpdateServerSettingsDto } from '../../entities/server-settings/update-server-setting.dto';
import { RavenInterceptor } from 'nest-raven';

@Controller('settings')
@UseInterceptors(
  new RavenInterceptor({
    filters: [
      {
        type: HttpException,
        filter: (exception: HttpException) => 499 < exception.getStatus(),
      },
    ],
  }),
)
export class SettingsController {
  constructor(
    private readonly settingsService: SettingsService,
    private readonly commandBus: CommandBus,
  ) {}

  @Get('v1/get')
  @Roles(ADMINISTRATOR)
  @UseGuards(TokenGuard, RoleGuard)
  async getSettings() {
    return await this.settingsService.find();
  }

  @Post('v1/update')
  @Roles(ADMINISTRATOR)
  @UseGuards(TokenGuard, RoleGuard)
  @UsePipes(ValidationPipe)
  async updateSettings(@Body() payload: UpdateServerSettingsDto) {
    return from(this.settingsService.find()).pipe(
      switchMap(settings => {
        return this.settingsService.update({ uuid: settings.uuid }, payload);
      }),
    );
  }

  @Post('v1/register_service')
  @Roles(ADMINISTRATOR)
  @UseGuards(TokenGuard, RoleGuard)
  async registerService(@Req() req) {
    const token = req.token;
    return await this.commandBus.execute(new RegisterServiceCommand(token));
  }

  @Post('v1/setup_permission_sync')
  @Roles(ADMINISTRATOR)
  @UseGuards(TokenGuard, RoleGuard)
  setupAutoSync(@Req() req) {
    return this.settingsService.setupPermissionsSync(req);
  }

  @Post('v1/update_client')
  @Roles(ADMINISTRATOR)
  @UseGuards(TokenGuard, RoleGuard)
  @UsePipes(new ValidationPipe())
  updateClient(@Body() body: UpdateServerSettingsDto) {
    return this.settingsService.updateClient(body);
  }
}
