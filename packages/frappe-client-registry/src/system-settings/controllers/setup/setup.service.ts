import {
  Injectable,
  HttpService,
  NotImplementedException,
} from '@nestjs/common';
import { settingsAlreadyExists } from '../../../constants/exceptions';
import { ServerSettings } from '../../../system-settings/entities/server-settings/server-settings.entity';
import { ServerSettingsService } from '../../../system-settings/entities/server-settings/server-settings.service';
import { switchMap } from 'rxjs/operators';
import {
  ConnectedServices,
  BASE_CLIENT_ROLE,
  FRAPPE_DECAF_SERVER_SERVICE_NAME,
} from '../../../constants/app-strings';
import { of, from, throwError } from 'rxjs';
import { ClientService } from '../../../client/entities/client/client.service';
import * as uuidv4 from 'uuid/v4';
import { Client } from '../../../client/entities/client/client.entity';
import { Role } from '../../../role/entity/role/role.entity';
import { RoleService } from '../../../role/entity/role/role.service';
import { GET_SERVICE_TYPE_ENDPOINT } from '../../../constants/url-endpoints';

@Injectable()
export class SetupService {
  setupData: { [key: string]: any } = {};
  constructor(
    protected readonly serverSettingsService: ServerSettingsService,
    protected readonly http: HttpService,
    protected readonly clientService: ClientService,
    private readonly roleService: RoleService,
  ) {}

  async setup(params) {
    if (await this.serverSettingsService.count()) {
      throw settingsAlreadyExists;
    }

    const settings: any = new ServerSettings();
    Object.assign(settings, params);
    delete settings.clientName;
    await settings.save();
    this.http
      .get(params.authServerURL + '/.well-known/openid-configuration')
      .pipe(
        switchMap(openIdConf => {
          this.setupData = openIdConf.data;
          return this.http.get(params.authServerURL + '/info');
        }),
        switchMap(authInfo => {
          const communicationService = authInfo.data.services.filter(
            service => service.type === ConnectedServices.CommunicationServer,
          );
          if (communicationService.length > 0) {
            this.setupData.communicationService = communicationService[0].url;
            settings.communicationServerURL = communicationService[0].url;
          }

          const infrastructureConsole = authInfo.data.services.filter(
            service => service.type === ConnectedServices.InfrastructureConsole,
          );
          if (infrastructureConsole.length > 0) {
            this.setupData.infrastructureConsole = infrastructureConsole[0].url;
            settings.infrastructureURL = infrastructureConsole[0].url;
          }

          const identityProvider = authInfo.data.services.filter(
            service => service.type === ConnectedServices.IdentityProvider,
          );
          if (identityProvider.length > 0) {
            this.setupData.identityProvider = identityProvider[0].url;
            settings.identityProviderURL = identityProvider[0].url;
          }
          return from(this.createClient(params)).pipe(
            switchMap(data => {
              delete params.clientName;
              this.setupData.clientUuid = data.uuid;
              return of(this.setupData);
            }),
          );
        }),
      )
      .subscribe({
        next: async response => {
          params.authorizationURL = response.authorization_endpoint;
          params.tokenURL = response.token_endpoint;
          params.profileURL = response.userinfo_endpoint;
          params.revocationURL = response.revocation_endpoint;
          params.clientUuid = response.clientUuid;
          params.introspectionURL = response.introspection_endpoint;
          params.callbackURLs = [
            params.appURL,
            params.appURL + '/index.html',
            params.appURL + '/silent-refresh',
          ];
          params.communicationService = response.communicationService;
          Object.assign(settings, params);
          await this.getServiceUrl(settings).toPromise();
          await settings.save();
        },
        error: async error => await settings.remove(),
      });
  }

  async getInfo() {
    const info = await this.serverSettingsService.find();
    if (info) {
      delete info.clientSecret, info._id;
    }
    return info;
  }

  async createClient(params) {
    const client = new Client();
    client.uuid = uuidv4();
    client.name = params.clientName;
    const savedClient = await this.clientService.create(client);
    const role = new Role();
    role.uuid = uuidv4();
    role.name = BASE_CLIENT_ROLE;
    await this.roleService.create(role);
    return savedClient;
  }

  async createRole(roleName) {
    const role = new Role();
    role.uuid = uuidv4();
    role.name = roleName;
    return await this.roleService.create(role);
  }

  getServiceUrl(settings: ServerSettings) {
    if (!settings.infrastructureURL) {
      return throwError(new NotImplementedException());
    }
    return this.http
      .get(
        settings.infrastructureURL +
          GET_SERVICE_TYPE_ENDPOINT +
          FRAPPE_DECAF_SERVER_SERVICE_NAME,
      )
      .pipe(
        switchMap(service => {
          if (service.data.length === 0) {
            return throwError(new NotImplementedException());
          }
          settings.decafServerUrl = service.data.docs[0].serviceURL;
          return of(settings.save());
        }),
      );
  }
}
