import { Injectable, BadRequestException } from '@nestjs/common';
import { throwError, Observable, of } from 'rxjs';
import { SETUP_ALREADY_COMPLETE } from '../../../constants/messages';
import { ClientTokenManagerService } from '../../../auth/aggregates/client-token-manager/client-token-manager.service';
import { switchMap } from 'rxjs/operators';
import { ServerSettingsService } from '../../../system-settings/entities/server-settings/server-settings.service';
import { ServerSettings } from '../../../system-settings/entities/server-settings/server-settings.entity';

@Injectable()
export class SettingsHelperService {
  constructor(
    private readonly clientTokenService: ClientTokenManagerService,
    private readonly settingService: ServerSettingsService,
  ) {}

  setupFrappeSystemManager(req, settings: Observable<ServerSettings>) {
    return settings.pipe(
      switchMap(serverSettings => {
        if (serverSettings.frappeSystemManagerUuid) {
          return throwError(new BadRequestException(SETUP_ALREADY_COMPLETE));
        }
        this.setupFrappeAdministrator(settings, serverSettings, req);
        return of({});
      }),
    );
  }

  setupFrappeAdministrator(
    settings: Observable<ServerSettings>,
    serverSettings: ServerSettings,
    req,
  ) {
    this.clientTokenService
      .getNewToken(settings)
      .pipe(
        switchMap(token => {
          return this.settingService.updateOne(
            { uuid: serverSettings.uuid },
            { $set: { frappeSystemManagerUuid: req.token.sub } },
          );
        }),
      )
      .subscribe({
        next: success => {},
        error: err => {},
      });
  }
}
