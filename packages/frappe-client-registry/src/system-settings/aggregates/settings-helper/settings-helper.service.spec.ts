import { Test, TestingModule } from '@nestjs/testing';
import { SettingsHelperService } from './settings-helper.service';
import { HttpModule } from '@nestjs/common';
import { TokenCacheService } from '../../../auth/entities/token-cache/token-cache.service';
import { ClientTokenManagerService } from '../../../auth/aggregates/client-token-manager/client-token-manager.service';
import { ServerSettingsService } from '../../../system-settings/entities/server-settings/server-settings.service';

describe('SettingsHelperService', () => {
  let service: SettingsHelperService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [HttpModule],
      providers: [
        SettingsHelperService,
        {
          provide: TokenCacheService,
          useValue: {},
        },
        {
          provide: ClientTokenManagerService,
          useValue: {},
        },
        {
          provide: ServerSettingsService,
          useValue: {},
        },
      ],
    }).compile();

    service = module.get<SettingsHelperService>(SettingsHelperService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
