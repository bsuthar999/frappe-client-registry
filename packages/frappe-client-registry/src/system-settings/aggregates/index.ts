import { SettingsService } from './settings/settings.service';
import { SettingsHelperService } from './settings-helper/settings-helper.service';

export const SystemSettingsAggregates = [
  SettingsService,
  SettingsHelperService,
];
