import { NestFactory } from '@nestjs/core';
import * as express from 'express';
import { ExpressAdapter } from '@nestjs/platform-express';
import { AppModule } from './app.module';
import { setupSwagger } from './swagger';
import { GLOBAL_API_PREFIX } from './constants/url-endpoints';
import { setupEventStore } from './event-store';
import { setupSentry } from './sentry';

async function bootstrap() {
  const server = new ExpressAdapter(express());
  const app = await NestFactory.create(AppModule, server);
  app.enableCors();
  setupSwagger(app);
  app.setGlobalPrefix(GLOBAL_API_PREFIX);
  setupEventStore(app);
  setupSentry(app);
  await app.listen(7000);
}
bootstrap();
