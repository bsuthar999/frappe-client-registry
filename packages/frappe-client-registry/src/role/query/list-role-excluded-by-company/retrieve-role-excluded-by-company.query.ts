import { IQuery } from '@nestjs/cqrs';

export class RetrieveRolesNotInCompanyQuery implements IQuery {
  constructor(
    public company: string,
    public offset: number,
    public limit: number,
    public search: string,
    public sort: string,
  ) {}
}
