import { IQueryHandler, QueryHandler } from '@nestjs/cqrs';
import { RetrieveRoleQuery } from './retrieve-role.query';
import { RoleAggregateService } from '../../aggregates/role-aggregate/role-aggregate.service';

@QueryHandler(RetrieveRoleQuery)
export class RetrieveRoleHandler implements IQueryHandler<RetrieveRoleQuery> {
  constructor(private readonly manager: RoleAggregateService) {}

  async execute(query: RetrieveRoleQuery) {
    const { req, uuid } = query;
    return this.manager.retrieveRole(uuid, req);
  }
}
