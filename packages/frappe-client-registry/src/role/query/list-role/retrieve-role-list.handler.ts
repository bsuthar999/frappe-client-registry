import { IQueryHandler, QueryHandler } from '@nestjs/cqrs';
import { RetrieveRoleListQuery } from './retrieve-role-list.query';
import { RoleAggregateService } from '../../aggregates/role-aggregate/role-aggregate.service';

@QueryHandler(RetrieveRoleListQuery)
export class RetrieveRoleListHandler
  implements IQueryHandler<RetrieveRoleListQuery> {
  constructor(private readonly manager: RoleAggregateService) {}
  async execute(query: RetrieveRoleListQuery) {
    const { offset, limit, search, sort } = query;
    return await this.manager.getRoleList(
      Number(offset),
      Number(limit),
      search,
      sort,
    );
  }
}
