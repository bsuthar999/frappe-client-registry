import { RetrieveRoleHandler } from './get-role/retrieve-role.handler';
import { RetrieveRoleListHandler } from './list-role/retrieve-role-list.handler';
import { RetrieveRolesNotInCompanyHandler } from './list-role-excluded-by-company/retrieve-role-excluded-by-company.handler';

export const QueryManager = [
  RetrieveRoleHandler,
  RetrieveRoleListHandler,
  RetrieveRolesNotInCompanyHandler,
];
