import { CommandHandler, ICommandHandler, EventPublisher } from '@nestjs/cqrs';
import { UpdateRoleCommand } from './update-role.command';
import { RoleAggregateService } from '../../aggregates/role-aggregate/role-aggregate.service';

@CommandHandler(UpdateRoleCommand)
export class UpdateRoleHandler implements ICommandHandler<UpdateRoleCommand> {
  constructor(
    private publisher: EventPublisher,
    private manager: RoleAggregateService,
  ) {}

  async execute(command: UpdateRoleCommand) {
    const { updatePayload } = command;
    const aggregate = this.publisher.mergeObjectContext(this.manager);
    await this.manager.update(updatePayload).toPromise();
    aggregate.commit();
  }
}
