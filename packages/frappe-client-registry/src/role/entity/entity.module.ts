import { Module, Global } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Role } from './role/role.entity';
import { RoleService } from './role/role.service';
import { CqrsModule } from '@nestjs/cqrs';

@Global()
@Module({
  imports: [TypeOrmModule.forFeature([Role]), CqrsModule],
  providers: [RoleService],
  exports: [RoleService],
})
export class RoleEntitiesModule {}
