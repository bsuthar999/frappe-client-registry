import { InjectRepository } from '@nestjs/typeorm';
import { Role } from './role.entity';
import { Injectable } from '@nestjs/common';
import { MongoRepository } from 'typeorm';

@Injectable()
export class RoleService {
  constructor(
    @InjectRepository(Role)
    private readonly roleRepository: MongoRepository<Role>,
  ) {}

  async create(rolePayload: Role) {
    const role = new Role();
    Object.assign(role, rolePayload);
    return await role.save();
  }

  async findOne(param, options?) {
    return await this.roleRepository.findOne(param, options);
  }

  async deleteOne(query, options?) {
    return await this.roleRepository.deleteOne(query, options);
  }

  async updateOne(query, options?) {
    return await this.roleRepository.updateOne(query, options);
  }

  async list(skip, take, search, sort) {
    const nameExp = new RegExp(search, 'i');
    const columns = this.roleRepository.manager.connection
      .getMetadata(Role)
      .ownColumns.map(column => column.propertyName);

    const $or = columns.map(field => {
      const filter = {};
      filter[field] = nameExp;
      return filter;
    });
    const $and: any[] = [{ $or }];

    const where: { $and: any } = { $and };

    const results = await this.roleRepository.find({
      skip,
      take,
      where,
    });

    return {
      docs: results || [],
      length: await this.roleRepository.count(where),
      offset: skip,
    };
  }

  async listByFilter(roleNames: string[], skip, take, search, sort) {
    const nameExp = new RegExp(search, 'i');
    const columns = this.roleRepository.manager.connection
      .getMetadata(Role)
      .ownColumns.map(column => column.propertyName);

    const $or = columns.map(field => {
      const filter = {};
      filter[field] = nameExp;
      return filter;
    });
    const $and: any[] = [{ $or }, { name: { $nin: roleNames } }];

    const where: { $and: any } = { $and };

    const results = await this.roleRepository.find({
      skip,
      take,
      where,
    });

    return {
      docs: results || [],
      length: await this.roleRepository.count(where),
      offset: skip,
    };
  }
}
