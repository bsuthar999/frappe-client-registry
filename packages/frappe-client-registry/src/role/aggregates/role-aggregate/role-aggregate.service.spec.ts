import { Test, TestingModule } from '@nestjs/testing';
import { RoleAggregateService } from './role-aggregate.service';
import { RoleService } from '../../entity/role/role.service';
import { RolePoliciesService } from '../../policies/role-policies/role-policies.service';
import { ClientRoleService } from '../../../client-role/entity/client-role/client-role.service';
describe('RoleAggregateService', () => {
  let service: RoleAggregateService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        RoleAggregateService,
        {
          provide: RoleService,
          useValue: {},
        },
        {
          provide: RolePoliciesService,
          useValue: {},
        },
        {
          provide: ClientRoleService,
          useValue: {},
        },
      ],
    }).compile();

    service = module.get<RoleAggregateService>(RoleAggregateService);
  });
  RoleAggregateService;
  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
