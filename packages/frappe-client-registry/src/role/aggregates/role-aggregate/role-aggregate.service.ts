import {
  Injectable,
  NotFoundException,
  BadRequestException,
} from '@nestjs/common';
import { AggregateRoot } from '@nestjs/cqrs';
import * as uuidv4 from 'uuid/v4';
import { RoleDto } from '../../entity/role/role-dto';
import { Role } from '../../entity/role/role.entity';
import { RoleService } from '../../entity/role/role.service';
import { RoleAddedEvent } from '../../event/role-added/role-added.event';
import { RoleRemovedEvent } from '../../event/role-removed/role-removed.event';
import { RoleUpdatedEvent } from '../../event/role-updated/role-updated.event';
import { UpdateRoleDto } from '../../entity/role/update-role.dto';
import { RolePoliciesService } from '../../policies/role-policies/role-policies.service';
import { switchMap } from 'rxjs/operators';
import { of, from, throwError } from 'rxjs';
import { ROLE_NOT_FOUND } from '../../../constants/messages';
import { ClientRoleService } from '../../../client-role/entity/client-role/client-role.service';

@Injectable()
export class RoleAggregateService extends AggregateRoot {
  constructor(
    private readonly roleService: RoleService,
    private readonly rolePolicyService: RolePoliciesService,
    private readonly clientRoleService: ClientRoleService,
  ) {
    super();
  }

  addRole(rolePayload: RoleDto, clientHttpRequest) {
    return this.rolePolicyService.validateRole(rolePayload).pipe(
      switchMap(existingRole => {
        const role = Object.assign(new Role(), rolePayload);
        role.uuid = uuidv4();
        this.apply(new RoleAddedEvent(role));
        return of({});
      }),
    );
  }

  async retrieveRole(uuid: string, req) {
    const provider = await this.roleService.findOne({ uuid });
    if (!provider) {
      throw new NotFoundException();
    }
    return provider;
  }

  async getRoleList(offset, limit, search, sort) {
    return this.roleService.list(offset, limit, search, sort);
  }

  removeRole(uuid: string) {
    return from(this.roleService.findOne({ uuid })).pipe(
      switchMap(role => {
        if (!role) {
          return throwError(new BadRequestException(ROLE_NOT_FOUND));
        }
        return this.rolePolicyService.validateClientRole(role).pipe(
          switchMap(success => {
            this.apply(new RoleRemovedEvent(role));
            return of({});
          }),
        );
      }),
    );
  }

  update(updatePayload: UpdateRoleDto) {
    return from(this.roleService.findOne({ uuid: updatePayload.uuid })).pipe(
      switchMap(role => {
        if (!role) {
          throw new NotFoundException(ROLE_NOT_FOUND);
        }
        return this.rolePolicyService.validateRole(updatePayload).pipe(
          switchMap(roleExists => {
            return this.rolePolicyService.validateClientRole(role).pipe(
              switchMap(success => {
                const updateRole = Object.assign(new Role(), updatePayload);
                this.apply(new RoleUpdatedEvent(updateRole));
                return of({});
              }),
            );
          }),
        );
      }),
    );
  }

  getRoleExcludedByCompany(company, offset, limit, search, sort) {
    return from(this.clientRoleService.find({ clientUuid: company })).pipe(
      switchMap(roleList => {
        if (!roleList) {
          return throwError(new BadRequestException(ROLE_NOT_FOUND));
        }
        const roleArray = roleList.map(role => role.name);
        return this.roleService.listByFilter(
          roleArray,
          offset,
          limit,
          search,
          sort,
        );
      }),
    );
  }
}
