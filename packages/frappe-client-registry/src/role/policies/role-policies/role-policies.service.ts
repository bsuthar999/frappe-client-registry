import { Injectable, BadRequestException } from '@nestjs/common';
import { RoleService } from '../../entity/role/role.service';
import { from, throwError, of } from 'rxjs';
import { switchMap } from 'rxjs/operators';
import { ROLE_ALREADY_EXIST, ROLE_IN_USE } from '../../../constants/messages';
import { ClientRoleService } from '../../../client-role/entity/client-role/client-role.service';

@Injectable()
export class RolePoliciesService {
  constructor(
    private readonly roleService: RoleService,
    private readonly clientRoleService: ClientRoleService,
  ) {}

  validateRole(rolePayload) {
    return from(this.roleService.findOne({ name: rolePayload.name })).pipe(
      switchMap(role => {
        if (role) {
          return throwError(new BadRequestException(ROLE_ALREADY_EXIST));
        }
        return of(true);
      }),
    );
  }

  validateClientRole(rolePayload) {
    return from(
      this.clientRoleService.findOne({ name: rolePayload.name }),
    ).pipe(
      switchMap(clientRole => {
        if (clientRole) {
          return throwError(new BadRequestException(ROLE_IN_USE));
        }
        return of(true);
      }),
    );
  }
}
