import { Test, TestingModule } from '@nestjs/testing';
import { RolePoliciesService } from './role-policies.service';
import { RoleService } from '../../entity/role/role.service';
import { ClientRoleService } from '../../../client-role/entity/client-role/client-role.service';

describe('RolePoliciesService', () => {
  let service: RolePoliciesService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        RolePoliciesService,
        {
          provide: RoleService,
          useValue: {},
        },
        {
          provide: ClientRoleService,
          useValue: {},
        },
      ],
    }).compile();

    service = module.get<RolePoliciesService>(RolePoliciesService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
