import { IEvent } from '@nestjs/cqrs';
import { Role } from '../../entity/role/role.entity';

export class RoleUpdatedEvent implements IEvent {
  constructor(public updatePayload: Role) {}
}
