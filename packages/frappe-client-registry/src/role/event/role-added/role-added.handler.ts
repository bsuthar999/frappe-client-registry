import { EventsHandler, IEventHandler } from '@nestjs/cqrs';
import { RoleService } from '../../entity/role/role.service';
import { RoleAddedEvent } from './role-added.event';

@EventsHandler(RoleAddedEvent)
export class RoleAddedHandler implements IEventHandler<RoleAddedEvent> {
  constructor(private readonly roleService: RoleService) {}
  async handle(event: RoleAddedEvent) {
    const { role } = event;
    await this.roleService.create(role);
  }
}
