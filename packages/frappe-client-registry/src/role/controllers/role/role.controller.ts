import {
  Controller,
  Post,
  UseGuards,
  UsePipes,
  Body,
  ValidationPipe,
  Req,
  Param,
  Get,
  Query,
  UseInterceptors,
  HttpException,
} from '@nestjs/common';
import { CommandBus, QueryBus } from '@nestjs/cqrs';
import { TokenGuard } from '../../../auth/guards/token.guard';
import { RoleDto } from '../../entity/role/role-dto';
import { AddRoleCommand } from '../../command/add-role/add-role.command';
import { RemoveRoleCommand } from '../../command/remove-role/remove-role.command';
import { UpdateRoleCommand } from '../../command/update-role/update-role.command';
import { UpdateRoleDto } from '../../entity/role/update-role.dto';
import { RetrieveRoleQuery } from '../../query/get-role/retrieve-role.query';
import { RetrieveRoleListQuery } from '../../query/list-role/retrieve-role-list.query';
import { RavenInterceptor } from 'nest-raven';
import { RetrieveRolesNotInCompanyQuery } from '../../query/list-role-excluded-by-company/retrieve-role-excluded-by-company.query';

@Controller('role')
@UseInterceptors(
  new RavenInterceptor({
    filters: [
      {
        type: HttpException,
        filter: (exception: HttpException) => 499 < exception.getStatus(),
      },
    ],
  }),
)
export class RoleController {
  constructor(
    private readonly commandBus: CommandBus,
    private readonly queryBus: QueryBus,
  ) {}

  @Post('v1/create')
  @UseGuards(TokenGuard)
  @UsePipes(new ValidationPipe({ whitelist: true }))
  create(@Body() rolePayload: RoleDto, @Req() req) {
    return this.commandBus.execute(new AddRoleCommand(rolePayload, req));
  }

  @Post('v1/remove/:uuid')
  @UseGuards(TokenGuard)
  remove(@Param('uuid') uuid) {
    return this.commandBus.execute(new RemoveRoleCommand(uuid));
  }

  @Get('v1/get/:uuid')
  @UseGuards(TokenGuard)
  get(@Param('uuid') uuid, @Req() req) {
    return this.queryBus.execute(new RetrieveRoleQuery(uuid, req));
  }

  @Get('v1/list')
  @UseGuards(TokenGuard)
  listRole(
    @Query('start') offset = 0,
    @Query('limit') limit = 10,
    @Query('search') search = '',
    @Query('sort') sort,
  ) {
    if (sort !== 'ASC') {
      sort = 'DESC';
    }
    return this.queryBus.execute(
      new RetrieveRoleListQuery(offset, limit, search, sort),
    );
  }

  @Post('v1/update')
  @UseGuards(TokenGuard)
  @UsePipes(new ValidationPipe({ whitelist: true }))
  updateClient(@Body() updatePayload: UpdateRoleDto) {
    return this.commandBus.execute(new UpdateRoleCommand(updatePayload));
  }

  @Get('v1/exclude_by_company')
  @UseGuards(TokenGuard)
  listExcludedUsers(
    @Query('company') company,
    @Query('start') offset = 0,
    @Query('limit') limit = 10,
    @Query('search') search = '',
    @Query('sort') sort,
  ) {
    if (sort !== 'ASC') {
      sort = 'DESC';
    }
    return this.queryBus.execute(
      new RetrieveRolesNotInCompanyQuery(company, offset, limit, search, sort),
    );
  }
}
