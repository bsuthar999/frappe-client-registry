import { Injectable, NotImplementedException } from '@nestjs/common';
import { UserService } from '../../../user/entities/user/user.service';
import { from, of, throwError } from 'rxjs';
import { User } from '../../../user/entities/user/user.entity';
import * as uuidV4 from 'uuid/v4';
import { switchMap } from 'rxjs/operators';

@Injectable()
export class UserInfoPoliciesService {
  constructor(private readonly userService: UserService) {}

  validateInfoUser(clientHttpRequest) {
    return from(
      this.userService.findOne({ email: clientHttpRequest.token.email }),
    ).pipe(
      switchMap(user => {
        if (!user) {
          return this.infoUserNotFound(clientHttpRequest);
        }
        return of(user);
      }),
    );
  }

  infoUserNotFound(clientHttpRequest) {
    const user = new User();
    user.uuid = uuidV4();
    user.name = clientHttpRequest.token.name;
    user.email = clientHttpRequest.token.email;
    return from(this.userService.create(user)).pipe(
      switchMap(userCreated => {
        if (!userCreated) {
          return throwError(new NotImplementedException());
        }
        return of({});
      }),
    );
  }
}
