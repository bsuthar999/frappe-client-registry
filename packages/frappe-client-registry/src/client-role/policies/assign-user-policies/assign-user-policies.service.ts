import { Injectable } from '@nestjs/common';
import { ClientRoleService } from '../../entity/client-role/client-role.service';

@Injectable()
export class AssignUserPoliciesService {
  constructor(private readonly clientRoleService: ClientRoleService) {}

  validateUsersInClient(clientRolePayload) {
    return this.clientRoleService.find({
      frappeClientUuid: clientRolePayload.frappeClientUuid,
      clientUuid: clientRolePayload.clientUuid,
      users: { $in: clientRolePayload.users },
      name: clientRolePayload.name,
    });
  }

  validateUser(clientRolePayload) {
    return this.clientRoleService.find({
      uuid: { $ne: clientRolePayload.uuid },
      users: { $in: clientRolePayload.users },
    });
  }
}
