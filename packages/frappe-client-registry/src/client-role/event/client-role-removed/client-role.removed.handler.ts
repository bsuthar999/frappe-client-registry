import { EventsHandler, IEventHandler } from '@nestjs/cqrs';
import { ClientRoleService } from '../../entity/client-role/client-role.service';
import { ClientRoleRemovedEvent } from './client-role-removed.event';

@EventsHandler(ClientRoleRemovedEvent)
export class ClientRoleRemovedHandler
  implements IEventHandler<ClientRoleRemovedEvent> {
  constructor(private readonly remove: ClientRoleService) {}
  async handle(event: ClientRoleRemovedEvent) {
    const { found } = event;
    await this.remove.removeClient(found);
  }
}
