import { Module, HttpModule } from '@nestjs/common';
import { ClientRoleAggregatesManager } from './aggregates';
import { QueryManager } from './query';
import { CqrsModule } from '@nestjs/cqrs';
import { CommandManager } from './command';
import { EventManager } from './event';
import { ClientRoleController } from './controllers/client-role/client-role.controller';
import { ClientRoleEntitiesModule } from './entity/client-role-entities.module';
import { ClientRolePoliciesService } from './policies/client-role-policies/client-role-policies.service';
import { RoleEntitiesModule } from '../role/entity/entity.module';
import { ClientModule } from '../client/client.module';
import { UserInfoPoliciesService } from './policies/user-info-policies/user-info-policies.service';
import { AssignUserPoliciesService } from './policies/assign-user-policies/assign-user-policies.service';
import { UserModule } from '../user/user.module';

@Module({
  imports: [
    ClientRoleEntitiesModule,
    CqrsModule,
    HttpModule,
    RoleEntitiesModule,
    ClientModule,
    UserModule,
  ],
  controllers: [ClientRoleController],
  providers: [
    ...ClientRoleAggregatesManager,
    ...QueryManager,
    ...EventManager,
    ...CommandManager,
    ClientRolePoliciesService,
    UserInfoPoliciesService,
    AssignUserPoliciesService,
  ],
  exports: [ClientRoleEntitiesModule, ...ClientRoleAggregatesManager],
})
export class ClientRoleModule {}
