import { QueryHandler, IQueryHandler } from '@nestjs/cqrs';
import { RetrieveUserInfoQuery } from './retrieve-user-info.query';
import { ClientRoleAggregateService } from '../../aggregates/client-role-aggregate/client-role-aggregate.service';

@QueryHandler(RetrieveUserInfoQuery)
export class RetrieveUserInfoHandler
  implements IQueryHandler<RetrieveUserInfoQuery> {
  constructor(private readonly manager: ClientRoleAggregateService) {}

  async execute(query: RetrieveUserInfoQuery) {
    const { clientHttpReq } = query;
    return this.manager.retrieveUserInfo(clientHttpReq).toPromise();
  }
}
