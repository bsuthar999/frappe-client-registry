import { IQuery } from '@nestjs/cqrs';

export class RetrieveClientRoleQuery implements IQuery {
  constructor(public readonly uuid: string, public readonly req: any) {}
}
