import { Test, TestingModule } from '@nestjs/testing';
import { ClientRoleAggregateService } from './client-role-aggregate.service';
import { ClientRoleService } from '../../entity/client-role/client-role.service';
import { ClientRolePoliciesService } from '../../policies/client-role-policies/client-role-policies.service';
import { ClientService } from '../../../client/entities/client/client.service';
import { UserInfoPoliciesService } from '../../policies/user-info-policies/user-info-policies.service';
import { AssignUserPoliciesService } from '../../policies/assign-user-policies/assign-user-policies.service';

describe('clientRoleAggregateService', () => {
  let service: ClientRoleAggregateService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        ClientRoleAggregateService,
        {
          provide: ClientRoleService,
          useValue: {},
        },
        {
          provide: ClientRolePoliciesService,
          useValue: {},
        },
        {
          provide: ClientService,
          useValue: {},
        },
        {
          provide: UserInfoPoliciesService,
          useValue: {},
        },
        {
          provide: AssignUserPoliciesService,
          useValue: {},
        },
      ],
    }).compile();

    service = module.get<ClientRoleAggregateService>(
      ClientRoleAggregateService,
    );
  });
  ClientRoleAggregateService;
  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
