import { ICommand } from '@nestjs/cqrs';

export class RemoveClientRoleCommand implements ICommand {
  constructor(public readonly uuid: string) {}
}
