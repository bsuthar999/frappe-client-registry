import { CommandHandler, EventPublisher, ICommandHandler } from '@nestjs/cqrs';
import { AssignNewUserCommand } from './assign-new-user.command';
import { ClientRoleAggregateService } from '../../aggregates/client-role-aggregate/client-role-aggregate.service';

@CommandHandler(AssignNewUserCommand)
export class AssignNewUserHandler
  implements ICommandHandler<AssignNewUserCommand> {
  constructor(
    private publisher: EventPublisher,
    private manager: ClientRoleAggregateService,
  ) {}
  async execute(command: AssignNewUserCommand) {
    const { userPayload: clientRolePayload, clientHttpRequest } = command;
    const aggregate = this.publisher.mergeObjectContext(this.manager);
    await aggregate
      .assignNewUser(clientRolePayload, clientHttpRequest)
      .toPromise();
    aggregate.commit();
  }
}
