import { ICommand } from '@nestjs/cqrs';
import { AssignUserDto } from '../../entity/client-role/assign-user-dto';

export class DeleteUserCommand implements ICommand {
  constructor(
    public readonly userPayload: AssignUserDto,
    public readonly clientHttpRequest: any,
  ) {}
}
