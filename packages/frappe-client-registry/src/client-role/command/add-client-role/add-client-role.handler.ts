import { CommandHandler, EventPublisher, ICommandHandler } from '@nestjs/cqrs';
import { AddClientRoleCommand } from './add-client-role.command';
import { ClientRoleAggregateService } from '../../aggregates/client-role-aggregate/client-role-aggregate.service';

@CommandHandler(AddClientRoleCommand)
export class AddClientRoleHandler
  implements ICommandHandler<AddClientRoleCommand> {
  constructor(
    private publisher: EventPublisher,
    private manager: ClientRoleAggregateService,
  ) {}
  async execute(command: AddClientRoleCommand) {
    const { clientRolePayload, clientHttpRequest } = command;
    const aggregate = this.publisher.mergeObjectContext(this.manager);
    await aggregate
      .addClientRole(clientRolePayload, clientHttpRequest)
      .toPromise();
    aggregate.commit();
  }
}
