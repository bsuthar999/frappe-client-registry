import { ICommand } from '@nestjs/cqrs';
import { ClientRoleDto } from '../../entity/client-role/client-role-dto';

export class AddClientRoleCommand implements ICommand {
  constructor(
    public clientRolePayload: ClientRoleDto,
    public readonly clientHttpRequest: any,
  ) {}
}
