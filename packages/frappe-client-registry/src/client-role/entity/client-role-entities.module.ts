import { Module, Global } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { CqrsModule } from '@nestjs/cqrs';
import { ClientRole } from './client-role/client-role.entity';
import { ClientRoleService } from './client-role/client-role.service';

@Global()
@Module({
  imports: [TypeOrmModule.forFeature([ClientRole]), CqrsModule],
  providers: [ClientRoleService],
  exports: [ClientRoleService],
})
export class ClientRoleEntitiesModule {}
