import { InjectRepository } from '@nestjs/typeorm';
import { ClientRole } from './client-role.entity';
import { Injectable } from '@nestjs/common';
import { MongoRepository } from 'typeorm';
import { of } from 'rxjs';
import { switchMap } from 'rxjs/operators';

@Injectable()
export class ClientRoleService {
  constructor(
    @InjectRepository(ClientRole)
    private readonly clientRoleRepository: MongoRepository<ClientRole>,
  ) {}

  async find(query?) {
    return await this.clientRoleRepository.find(query);
  }

  async create(clientRolePayload: ClientRole) {
    const clientRolePayloadObject = new ClientRole();
    Object.assign(clientRolePayloadObject, clientRolePayload);
    return await clientRolePayloadObject.save();
  }

  async findOne(query, options?) {
    return await this.clientRoleRepository.findOne(query, options);
  }

  async list(skip, take, search, sort) {
    const nameExp = new RegExp(search, 'i');
    const columns = this.clientRoleRepository.manager.connection
      .getMetadata(ClientRole)
      .ownColumns.map(column => column.propertyName);

    const $or = columns.map(field => {
      const filter = {};
      filter[field] = nameExp;
      return filter;
    });
    const $and: any[] = [{ $or }];

    const where: { $and: any } = { $and };

    const results = await this.clientRoleRepository.find({
      skip,
      take,
      where,
    });

    return {
      docs: results || [],
      length: await this.clientRoleRepository.count(where),
      offset: skip,
    };
  }

  async deleteProfile(uuid) {
    return await this.clientRoleRepository.delete({ uuid });
  }

  async removeClient(provider: ClientRole) {
    return await provider.remove();
  }

  async updateOne(query, updateOptions) {
    return await this.clientRoleRepository.updateOne(query, updateOptions);
  }

  aggregate(query) {
    return this.clientRoleRepository.aggregate(query);
  }

  asyncAggregate(query) {
    return of(this.aggregate(query)).pipe(
      switchMap((aggregateData: any) => {
        return aggregateData.toArray();
      }),
    );
  }
}
