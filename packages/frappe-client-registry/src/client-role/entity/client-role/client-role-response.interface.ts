export interface ClientRoleResponseInterface {
  name: string;
  email: string;
  instances: FrappeInstanceUserInfoMetaData[];
}

export class FrappeInstanceUserInfoMetaData {
  frappeClientUuid: string;
  clientUuid: string;
  clientName?: string;
  instanceName?: string;
  roles: string[];
}
