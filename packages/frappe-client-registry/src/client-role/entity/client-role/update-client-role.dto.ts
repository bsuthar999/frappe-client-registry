import { IsOptional, IsNotEmpty } from 'class-validator';

export class UpdateClientRoleDto {
  @IsNotEmpty()
  uuid: string;

  @IsOptional()
  name: string;

  @IsOptional()
  users: string[];
}
