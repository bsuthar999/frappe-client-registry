import { IsNotEmpty, IsArray } from 'class-validator';

export class AssignUserDto {
  @IsNotEmpty()
  uuid: string;

  @IsNotEmpty()
  frappeClientUuid: string;

  @IsNotEmpty()
  @IsArray()
  users: string[];
}
