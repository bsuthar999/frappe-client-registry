export const SETUP_ALREADY_COMPLETE = 'Setup already complete';
export const PLEASE_RUN_SETUP = 'Please run setup';
export const SOMETHING_WENT_WRONG = 'Something went wrong';
export const NOT_CONNECTED = 'not connected';
export const SERVICE_ALREADY_REGISTERED = 'Service already registered';
export const CLIENT_NOT_FOUND = 'Client not found';
export const FRAPPE_API_KEY_EXISTS =
  'FrappeApiKey for frappeInstance already Exists';
export const FRAPPE_API_KEY_DOES_NOT_EXISTS =
  'FrappeApiKey for frappeInstance does Not Exists';
export const FRAPPE_CLIENT_NOT_FOUND = 'Frappe Client not found on decaf';
export const PLEASE_REVOKE_TOKENS = ' Please revoke tokens to re-run setup';
export const USER_NOT_FOUND = 'User not found';
export const USER_PROFILE_MISSING = 'User profile missing';
export const SERVER_REQUEST_ENDPOINT =
  'Only a server must request this endpoint.';
export const CLIENT_ALREADY_EXIST = 'Client already exists';
export const CLIENT_ROLE_ALREADY_EXIST = 'Client-role already exists';
export const USER_ALREADY_EXIST = 'User already exists';
export const USER_CANNOT_BE_PART_OF_MULTIPLE_COMPANY =
  'User Cannot be a part of Multiple company';
export const ROLE_ALREADY_EXIST = 'Role already exists';
export const ROLE_IN_USE =
  'Role cannot be updated as Client-role exists for this role';
export const ROLE_NOT_FOUND = 'Role Not Found';
export const USER_DOES_NOT_EXIST = 'User Does Not Exists';
export const INVALID_USER_IN_ARRAY = 'Invalid user is provided';
export const INVALID_ROLE = 'Role Does not exist';
export const COMPANY_ROLE_NOT_FOUND = 'Company-Role does not exist';
export const SETUP_PERMISSION_SYNC_INCOMPLETE =
  'Please ask administrator to setup permission sync';
export const FRAPPE_INSTANCE_NAME_ALREADY_EXIST =
  'Frappe Instance should be unique, instance with provided credentials already exist';
export const FRAPPE_INSTANCE_ALREADY_EXIST =
  'Frappe Instance should be unique, provided instance already exist';
export const FRAPPE_INSTANCE_DOES_NOT_EXIST = 'Frappe Instance does not exist';
