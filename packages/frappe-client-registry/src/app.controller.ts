import {
  Controller,
  Get,
  UseInterceptors,
  HttpException,
} from '@nestjs/common';
import { AppService } from './app.service';
import { RavenInterceptor } from 'nest-raven';

@Controller()
@UseInterceptors(
  new RavenInterceptor({
    filters: [
      {
        type: HttpException,
        filter: (exception: HttpException) => 499 < exception.getStatus(),
      },
    ],
  }),
)
export class AppController {
  constructor(private readonly appService: AppService) {}

  @Get()
  getRoot() {
    return this.appService.getRoot();
  }

  @Get('info')
  info() {
    return this.appService.info();
  }
}
