import { IsNotEmpty } from 'class-validator';

export class UpdateClientDto {
  @IsNotEmpty()
  uuid: string;

  @IsNotEmpty()
  name: string;
}
