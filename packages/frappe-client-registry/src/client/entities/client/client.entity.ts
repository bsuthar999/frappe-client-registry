import { Entity, BaseEntity, ObjectIdColumn, ObjectID, Column } from 'typeorm';

@Entity()
export class Client extends BaseEntity {
  @ObjectIdColumn()
  _id: ObjectID;

  @Column()
  uuid: string;

  @Column()
  name: string;

  @Column()
  frappeInstance: FrappeInstanceMetadata[];
}

export class FrappeInstanceMetadata {
  frappeClientUuid: string;
  clientUuid: string;
  name: string;
  frappeApiKey: string;
  bloomstackBotPassword: string;
}
