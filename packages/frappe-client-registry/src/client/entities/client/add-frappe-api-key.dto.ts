import { IsNotEmpty, IsOptional } from 'class-validator';

export class AddFrappeAPIKeyDto {
  @IsNotEmpty()
  frappeClientUuid: string;

  @IsNotEmpty()
  clientUuid: string;

  @IsOptional()
  frappeApiKey: string;
}
