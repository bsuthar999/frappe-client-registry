import { CommandHandler, ICommandHandler, EventPublisher } from '@nestjs/cqrs';
import { AddNewFrappeInstanceCommand } from './add-new-frappe-instance.command';
import { ClientAggregateService } from '../../aggregates/client-aggregate/client-aggregate.service';

@CommandHandler(AddNewFrappeInstanceCommand)
export class AddNewFrappeInstanceHandler
  implements ICommandHandler<AddNewFrappeInstanceCommand> {
  constructor(
    private publisher: EventPublisher,
    private manager: ClientAggregateService,
  ) {}

  async execute(command: AddNewFrappeInstanceCommand) {
    const { clientPayload } = command;
    const aggregate = this.publisher.mergeObjectContext(this.manager);
    await aggregate.addNewFrappeInstance(clientPayload).toPromise();
    aggregate.commit();
  }
}
