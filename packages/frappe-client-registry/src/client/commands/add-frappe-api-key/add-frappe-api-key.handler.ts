import { CommandHandler, ICommandHandler, EventPublisher } from '@nestjs/cqrs';
import { ClientAggregateService } from '../../aggregates/client-aggregate/client-aggregate.service';
import { AddFrappeAPIKeyCommand } from './add-frappe-api-key.command';

@CommandHandler(AddFrappeAPIKeyCommand)
export class AddFrappeAPIKeyHandler
  implements ICommandHandler<AddFrappeAPIKeyCommand> {
  constructor(
    private publisher: EventPublisher,
    private manager: ClientAggregateService,
  ) {}

  async execute(command: AddFrappeAPIKeyCommand) {
    const { clientPayload } = command;
    const aggregate = this.publisher.mergeObjectContext(this.manager);
    await aggregate.addFrappeAPIKey(clientPayload).toPromise();
    aggregate.commit();
  }
}
