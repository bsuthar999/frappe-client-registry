import { ICommand } from '@nestjs/cqrs';
import { UpdateClientDto } from '../../entities/client/update-client.dto';

export class UpdateClientCommand implements ICommand {
  constructor(public readonly clientPayload: UpdateClientDto) {}
}
