import { ICommand } from '@nestjs/cqrs';
import { ClientDto } from '../../entities/client/client.dto';

export class AddNewClientCommand implements ICommand {
  constructor(public clientPayload: ClientDto) {}
}
