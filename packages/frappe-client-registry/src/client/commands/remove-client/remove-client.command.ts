import { ICommand } from '@nestjs/cqrs';

export class RemoveClientCommand implements ICommand {
  constructor(public readonly uuid: string) {}
}
