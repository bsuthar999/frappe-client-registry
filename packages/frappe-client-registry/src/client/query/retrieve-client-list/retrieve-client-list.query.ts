import { IQuery } from '@nestjs/cqrs';

export class RetrieveClientListQuery implements IQuery {
  constructor(
    public offset: number,
    public limit: number,
    public sort: string,
    public search: string,
    public clientHttpRequest: any,
  ) {}
}
