import { IQuery } from '@nestjs/cqrs';

export class RetrieveFrappeInstanceAsServerQuery implements IQuery {
  constructor(
    public readonly clientUuid: string,
    public readonly frappeClientUuid: string,
  ) {}
}
