import { IQueryHandler, QueryHandler } from '@nestjs/cqrs';
import { ClientAggregateService } from '../../aggregates/client-aggregate/client-aggregate.service';
import { RetrieveFrappeInstanceAsServerQuery } from './retrieve-frappe-instance-as-server.query';

@QueryHandler(RetrieveFrappeInstanceAsServerQuery)
export class RetrieveFrappeInstanceAsServerHandler
  implements IQueryHandler<RetrieveFrappeInstanceAsServerQuery> {
  constructor(private manager: ClientAggregateService) {}

  async execute(query: RetrieveFrappeInstanceAsServerQuery) {
    const { clientUuid, frappeClientUuid } = query;
    return await this.manager.retrieveFrappeInstanceAsServer(
      clientUuid,
      frappeClientUuid,
    );
  }
}
