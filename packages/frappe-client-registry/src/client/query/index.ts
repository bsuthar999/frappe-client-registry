import { RetrieveClientListHandler } from './retrieve-client-list/retrieve-client-list.handler';
import { RetrieveClientHandler } from './retrieve-client/retrieve-client.handler';
import { RetrieveFrappeInstanceHandler } from './retrieve-frappe-instance/retrieve-frappe-instance.handler';
import { RetrieveFrappeInstanceAsServerHandler } from './retrieve-frappe-instance-as-server/retrieve-frappe-instance-as-server.handler';

export const QueryManager = [
  RetrieveClientListHandler,
  RetrieveClientHandler,
  RetrieveFrappeInstanceHandler,
  RetrieveFrappeInstanceAsServerHandler,
];
