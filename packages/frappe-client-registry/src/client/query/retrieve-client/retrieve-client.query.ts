import { IQuery } from '@nestjs/cqrs';

export class RetrieveClientQuery implements IQuery {
  constructor(
    public readonly uuid: string,
    public readonly clientHttpRequest: any,
  ) {}
}
