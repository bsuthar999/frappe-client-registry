import { IQuery } from '@nestjs/cqrs';

export class RetrieveFrappeInstanceQuery implements IQuery {
  constructor(
    public readonly clientUuid: string,
    public readonly frappeClientUuid: string,
  ) {}
}
