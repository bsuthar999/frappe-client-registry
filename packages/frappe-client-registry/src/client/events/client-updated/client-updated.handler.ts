import { EventsHandler, IEventHandler } from '@nestjs/cqrs';
import { ClientUpdatedEvent } from './client-updated.event';
import { ClientService } from '../../entities/client/client.service';

@EventsHandler(ClientUpdatedEvent)
export class ClientUpdatedHandler implements IEventHandler<ClientUpdatedEvent> {
  constructor(private readonly clientService: ClientService) {}

  async handle(event: ClientUpdatedEvent) {
    const { clientPayload } = event;
    await this.clientService.updateClient(clientPayload);
  }
}
