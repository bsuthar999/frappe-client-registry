import { EventsHandler, IEventHandler } from '@nestjs/cqrs';
import { ClientService } from '../../entities/client/client.service';
import { FrappeAPIKeyAddedEvent } from './frappe-api-key-added.event';
import { UUID } from '../../../constants/app-strings';

@EventsHandler(FrappeAPIKeyAddedEvent)
export class FrappeAPIKeyAddedHandler
  implements IEventHandler<FrappeAPIKeyAddedEvent> {
  constructor(private readonly clientService: ClientService) {}

  async handle(event: FrappeAPIKeyAddedEvent) {
    const { clientPayload } = event;
    const query = {
      'frappeInstance.frappeClientUuid': clientPayload.frappeClientUuid,
    };

    query[UUID] = clientPayload.clientUuid;

    await this.clientService.updateOne(query, {
      $set: { 'frappeInstance.$.frappeApiKey': clientPayload.frappeApiKey },
    });
  }
}
