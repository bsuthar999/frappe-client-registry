import { EventsHandler, IEventHandler } from '@nestjs/cqrs';
import { FrappeInstanceAddedEvent } from './frappe-instance-added.event';
import { ClientService } from '../../entities/client/client.service';

@EventsHandler(FrappeInstanceAddedEvent)
export class FrappeInstanceAddedHandler
  implements IEventHandler<FrappeInstanceAddedEvent> {
  constructor(private readonly clientService: ClientService) {}

  async handle(event: FrappeInstanceAddedEvent) {
    const { frappeInstance } = event;
    await this.clientService.updateOne(
      { uuid: frappeInstance.clientUuid },
      { $push: { frappeInstance } },
    );
  }
}
