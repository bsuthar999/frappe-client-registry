import { Test, TestingModule } from '@nestjs/testing';
import { ClientAggregateService } from './client-aggregate.service';
import { ClientService } from '../../entities/client/client.service';
import { ClientPoliciesService } from '../../policies/client-policies/client-policies.service';
import { FrappeInstancePoliciesService } from '../../policies/frappe-instance-policies/frappe-instance-policies.service';

describe('ClientAggregateService', () => {
  let service: ClientAggregateService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        ClientAggregateService,
        {
          provide: ClientService,
          useValue: {},
        },
        {
          provide: ClientPoliciesService,
          useValue: {},
        },
        {
          provide: FrappeInstancePoliciesService,
          useValue: {},
        },
      ],
    }).compile();

    service = module.get<ClientAggregateService>(ClientAggregateService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
