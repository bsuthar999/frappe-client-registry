import {
  Injectable,
  NotFoundException,
  BadRequestException,
} from '@nestjs/common';
import * as uuidv4 from 'uuid/v4';
import { AggregateRoot } from '@nestjs/cqrs';
import { ClientAddedEvent } from '../../events/client-added/client-added.event';
import { ClientUpdatedEvent } from '../../events/client-updated/client-updated.event';
import { UpdateClientDto } from '../../entities/client/update-client.dto';
import { ClientDto } from '../../entities/client/client.dto';
import { Client } from '../../entities/client/client.entity';
import { ClientService } from '../../entities/client/client.service';
import { ClientRemovedEvent } from '../../events/client-removed/client-removed.event';
import {
  CLIENT_NOT_FOUND,
  FRAPPE_API_KEY_EXISTS,
  FRAPPE_INSTANCE_DOES_NOT_EXIST,
  FRAPPE_API_KEY_DOES_NOT_EXISTS,
} from '../../../constants/messages';
import { ClientPoliciesService } from '../../policies/client-policies/client-policies.service';
import { switchMap } from 'rxjs/operators';
import { of, from, throwError } from 'rxjs';
import { AddNewFrappeInstanceDto } from '../../entities/client/add-frappe-instance.dto';
import { FrappeInstancePoliciesService } from '../../policies/frappe-instance-policies/frappe-instance-policies.service';
import { FrappeInstanceAddedEvent } from '../../events/frappe-instance-added/frappe-instance-added.event';
import { AddFrappeAPIKeyDto } from '../../../client/entities/client/add-frappe-api-key.dto';
import { FrappeAPIKeyAddedEvent } from '../../../client/events/frappe-api-key-added/frappe-api-key-added.event';
import { randomBytes } from 'crypto';
import { UUID } from '../../../constants/app-strings';
import { FrappeAPIKeyUpdatedEvent } from '../../../client/events/frappe-api-key-updated/frappe-api-key-updated.event';

@Injectable()
export class ClientAggregateService extends AggregateRoot {
  constructor(
    private readonly clientService: ClientService,
    private readonly clientPolicyService: ClientPoliciesService,
    private readonly frappeInstancePolicyService: FrappeInstancePoliciesService,
  ) {
    super();
  }

  addNewClient(clientPayload: ClientDto) {
    return this.clientPolicyService.validateClient(clientPayload).pipe(
      switchMap(existingClient => {
        const client = Object.assign(new Client(), clientPayload);
        client.uuid = uuidv4();
        this.apply(new ClientAddedEvent(client));
        return of({});
      }),
    );
  }

  updateClient(clientPayload: UpdateClientDto) {
    return this.clientPolicyService.validateClient(clientPayload).pipe(
      switchMap(existingClient => {
        return from(
          this.clientService.findOne({ uuid: clientPayload.uuid }),
        ).pipe(
          switchMap(foundClient => {
            if (!foundClient) {
              return throwError(new NotFoundException(CLIENT_NOT_FOUND));
            }
            const client = Object.assign(new Client(), clientPayload);
            this.apply(new ClientUpdatedEvent(client));
            return of({});
          }),
        );
      }),
    );
  }

  async removeClient(uuid: string) {
    const client = await this.clientService.findOne({ uuid });
    if (!client) {
      throw new NotFoundException(CLIENT_NOT_FOUND);
    }
    this.apply(new ClientRemovedEvent(client));
  }

  getClientList(offset, limit, sort, search, clientHttpRequest) {
    return this.clientService.list(offset, limit, search, sort);
  }

  async retrieveClient(uuid: string, clientHttpRequest) {
    const client = await this.clientService.findOne({ uuid });
    if (!client) {
      throw new NotFoundException(CLIENT_NOT_FOUND);
    }
    const query = {
      uuid,
    };
    return this.clientService
      .asyncAggregate([
        {
          $match: query,
        },
        {
          $project: {
            frappeInstance: {
              bloomstackBotPassword: 0,
              frappeApiKey: 0,
            },
          },
        },
      ])
      .pipe(
        switchMap((frappeInstance: any) => {
          return of(frappeInstance);
        }),
      );
  }

  retrieveFrappeInstance(clientUuid: string, frappeClientUuid) {
    const query = {
      'frappeInstance.frappeClientUuid': frappeClientUuid,
    };

    query[UUID] = clientUuid;
    return this.clientService
      .asyncAggregate([
        {
          $match: query,
        },
        {
          $project: {
            _id: 0,
            frappeInstance: {
              $filter: {
                input: '$frappeInstance',
                as: 'frappeInstance',
                cond: {
                  $eq: ['$$frappeInstance.frappeClientUuid', frappeClientUuid],
                },
              },
            },
          },
        },
        { $unwind: '$frappeInstance' },
        {
          $project: {
            frappeInstance: {
              bloomstackBotPassword: 0,
              frappeApiKey: 0,
            },
          },
        },
      ])
      .pipe(
        switchMap((frappeInstance: any) => {
          if (!frappeInstance || frappeInstance.length === 0) {
            return throwError(
              new BadRequestException(FRAPPE_INSTANCE_DOES_NOT_EXIST),
            );
          }
          return of(frappeInstance[0]);
        }),
      );
  }
  retrieveFrappeInstanceAsServer(clientUuid: string, frappeClientUuid) {
    const query = {
      'frappeInstance.frappeClientUuid': frappeClientUuid,
    };

    query[UUID] = clientUuid;
    return this.clientService
      .asyncAggregate([
        {
          $match: query,
        },
        {
          $project: {
            _id: 0,
            name: 1,
            frappeInstance: {
              $filter: {
                input: '$frappeInstance',
                as: 'frappeInstance',
                cond: {
                  $eq: ['$$frappeInstance.frappeClientUuid', frappeClientUuid],
                },
              },
            },
          },
        },
        { $unwind: '$frappeInstance' },
        {
          $project: {
            frappeInstance: {
              bloomstackBotPassword: 1,
            },
          },
        },
      ])
      .pipe(
        switchMap((frappeInstance: any) => {
          if (!frappeInstance || frappeInstance.length === 0) {
            return throwError(
              new BadRequestException(FRAPPE_INSTANCE_DOES_NOT_EXIST),
            );
          }
          return of(frappeInstance[0]);
        }),
      );
  }

  addNewFrappeInstance(clientPayload: AddNewFrappeInstanceDto) {
    return from(
      this.clientService.findOne({ uuid: clientPayload.clientUuid }),
    ).pipe(
      switchMap(client => {
        if (!client) {
          return throwError(new NotFoundException(CLIENT_NOT_FOUND));
        }
        return this.frappeInstancePolicyService
          .validateFrappeInstance(clientPayload)
          .pipe(
            switchMap(isValid => {
              this.apply(new FrappeInstanceAddedEvent(clientPayload));
              return of({});
            }),
          );
      }),
    );
  }

  addFrappeAPIKey(clientPayload: AddFrappeAPIKeyDto) {
    return from(
      this.clientService.findOne({ uuid: clientPayload.clientUuid }),
    ).pipe(
      switchMap(client => {
        if (!client) {
          return throwError(new NotFoundException(CLIENT_NOT_FOUND));
        }
        const query = {
          'frappeInstance.frappeClientUuid': clientPayload.frappeClientUuid,
        };

        query[UUID] = clientPayload.clientUuid;
        return this.clientService
          .asyncAggregate([
            {
              $match: query,
            },
          ])
          .pipe(
            switchMap((instance: any) => {
              if (!instance || instance.length === 0) {
                return throwError(
                  new BadRequestException(FRAPPE_INSTANCE_DOES_NOT_EXIST),
                );
              }
              const queryInstance = {
                'frappeInstance.frappeClientUuid':
                  clientPayload.frappeClientUuid,
              };

              queryInstance[UUID] = clientPayload.clientUuid;
              return this.clientService
                .asyncAggregate([
                  {
                    $match: queryInstance,
                  },
                  {
                    $project: {
                      _id: 0,
                      frappeInstance: {
                        $filter: {
                          input: '$frappeInstance',
                          as: 'frappeInstance',
                          cond: {
                            $eq: [
                              '$$frappeInstance.frappeClientUuid',
                              clientPayload.frappeClientUuid,
                            ],
                          },
                        },
                      },
                    },
                  },
                  { $unwind: '$frappeInstance' },
                ])
                .pipe(
                  switchMap((foundInstance: any) => {
                    if (foundInstance[0].frappeInstance.frappeApiKey) {
                      return throwError(
                        new BadRequestException(FRAPPE_API_KEY_EXISTS),
                      );
                    }
                    clientPayload.frappeApiKey = this.randomBytes32();
                    this.apply(new FrappeAPIKeyAddedEvent(clientPayload));
                    return of({});
                  }),
                );
            }),
          );
      }),
    );
  }

  updateFrappeApiKey(clientPayload: AddFrappeAPIKeyDto) {
    return from(
      this.clientService.findOne({ uuid: clientPayload.clientUuid }),
    ).pipe(
      switchMap(client => {
        if (!client) {
          return throwError(new NotFoundException(CLIENT_NOT_FOUND));
        }
        const query = {
          'frappeInstance.frappeClientUuid': clientPayload.frappeClientUuid,
        };

        query[UUID] = clientPayload.clientUuid;
        return this.clientService
          .asyncAggregate([
            {
              $match: query,
            },
            {
              $project: {
                _id: 0,
                frappeInstance: {
                  $filter: {
                    input: '$frappeInstance',
                    as: 'frappeInstance',
                    cond: {
                      $eq: [
                        '$$frappeInstance.frappeClientUuid',
                        clientPayload.frappeClientUuid,
                      ],
                    },
                  },
                },
              },
            },
            { $unwind: '$frappeInstance' },
          ])
          .pipe(
            switchMap((instance: any) => {
              if (!instance || instance.length === 0) {
                return throwError(
                  new BadRequestException(FRAPPE_INSTANCE_DOES_NOT_EXIST),
                );
              }
              const queryInstance = {
                'frappeInstance.frappeClientUuid':
                  clientPayload.frappeClientUuid,
              };

              queryInstance[UUID] = clientPayload.clientUuid;
              return this.clientService
                .asyncAggregate([
                  {
                    $match: queryInstance,
                  },
                  {
                    $project: {
                      _id: 0,
                      frappeInstance: {
                        $filter: {
                          input: '$frappeInstance',
                          as: 'frappeInstance',
                          cond: {
                            $eq: [
                              '$$frappeInstance.frappeClientUuid',
                              clientPayload.frappeClientUuid,
                            ],
                          },
                        },
                      },
                    },
                  },
                  { $unwind: '$frappeInstance' },
                ])
                .pipe(
                  switchMap((foundInstance: any) => {
                    if (!foundInstance[0].frappeInstance.frappeApiKey) {
                      return throwError(
                        new BadRequestException(FRAPPE_API_KEY_DOES_NOT_EXISTS),
                      );
                    }
                    clientPayload.frappeApiKey = this.randomBytes32();
                    this.apply(new FrappeAPIKeyUpdatedEvent(clientPayload));
                    return of({});
                  }),
                );
            }),
          );
      }),
    );
  }

  randomBytes32() {
    return randomBytes(32).toString('hex');
  }
}
