import {
  CanActivate,
  ExecutionContext,
  ForbiddenException,
  Injectable,
} from '@nestjs/common';
import { SERVER_REQUEST_ENDPOINT } from '../../constants/messages';

@Injectable()
export class ServerGuard implements CanActivate {
  canActivate(context: ExecutionContext) {
    const httpContext = context.switchToHttp();
    const req = httpContext.getRequest();

    if (!req.token.trustedClient || req.token.sub) {
      throw new ForbiddenException(SERVER_REQUEST_ENDPOINT);
    }
    return true;
  }
}
