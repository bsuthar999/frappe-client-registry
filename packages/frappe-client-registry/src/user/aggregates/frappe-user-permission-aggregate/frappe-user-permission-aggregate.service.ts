import {
  Injectable,
  HttpService,
  NotImplementedException,
} from '@nestjs/common';
import { switchMap } from 'rxjs/operators';
import { of, from, throwError } from 'rxjs';
import { SETUP_PERMISSION_SYNC_INCOMPLETE } from '../../../constants/messages';
import {
  SERVICE_RETRIEVE_ENDPOINT,
  ERP_GET_ROLE_ENDPOINT,
  ERP_SET_ROLE_ENDPOINT,
  FRAPPE_CONNECTOR_TRUSTED_CLIENT_ENDPOINT,
} from '../../../constants/url-endpoints';
import {
  SUPPORT_SERVER_SERVICE,
  BEARER,
  AUTHORIZATION,
  CONTENT_TYPE,
  ACCEPT,
  APPLICATION_JSON_HEADER,
  FRAPPE_FAILURE_RETRY_COUNT,
} from '../../../constants/app-strings';
import { ServerSettings } from '../../../system-settings/entities/server-settings/server-settings.entity';
import { TokenCache } from '../../../auth/entities/token-cache/token-cache.entity';
import { ERP_SYSTEM_MANAGER } from '../../../constants/filesystem';
import { ServerSettingsService } from '../../../system-settings/entities/server-settings/server-settings.service';
import { ClientTokenManagerService } from '../../../auth/aggregates/client-token-manager/client-token-manager.service';
import { RecursiveSyncService } from '../../../frappe-sync/aggregates/recursive-sync/recursive-sync.service';
import { HttpMethod } from '../../../constants/request-methods';
import { OutgoingHttpHeaders } from 'http';

@Injectable()
export class FrappeUserPermissionAggregateService {
  constructor(
    private readonly http: HttpService,
    private readonly settingService: ServerSettingsService,
    private readonly clientTokenService: ClientTokenManagerService,
    private readonly recursiveSyncService: RecursiveSyncService,
  ) {}

  setupFrappeRole(email: string) {
    return from(this.settingService.find())
      .pipe(
        switchMap((serverSettings: any) => {
          if (serverSettings.infrastructureURL) {
            return this.http
              .get(
                serverSettings.infrastructureURL +
                  SERVICE_RETRIEVE_ENDPOINT +
                  SUPPORT_SERVER_SERVICE,
              )
              .pipe(
                switchMap((services: any) => {
                  if (services.length === 0) {
                    return throwError(
                      new NotImplementedException(
                        SETUP_PERMISSION_SYNC_INCOMPLETE,
                      ),
                    );
                  }
                  const serviceUrl = services.data.docs[0].serviceURL;
                  return this.http.get(serviceUrl + '/api/info').pipe(
                    switchMap((supportInfo: any) => {
                      if (!supportInfo.data.frappeServerUuid) {
                        return throwError(new NotImplementedException());
                      }
                      return this.getFrappeRole(
                        supportInfo.data,
                        email,
                        serverSettings,
                      );
                    }),
                  );
                }),
              );
          }
          return throwError(new NotImplementedException());
        }),
      )
      .subscribe({
        next: success => {},
        error: err => {},
      });
  }

  getFrappeRole(
    supportInfo: { frappeConnectorURL: string; frappeServerUuid: string },
    email: string,
    serverSettings: ServerSettings,
  ) {
    return this.clientTokenService.getClientToken().pipe(
      switchMap(token => {
        const url =
          supportInfo.frappeConnectorURL +
          FRAPPE_CONNECTOR_TRUSTED_CLIENT_ENDPOINT +
          supportInfo.frappeServerUuid +
          `/${serverSettings.frappeSystemManagerUuid}` +
          ERP_GET_ROLE_ENDPOINT +
          `?uid=${email}`;

        const headers: any = {};
        headers[AUTHORIZATION] = BEARER + token.accessToken;
        headers[CONTENT_TYPE] = APPLICATION_JSON_HEADER;
        headers[ACCEPT] = APPLICATION_JSON_HEADER;

        const callbackParams = { supportInfo, token, email, serverSettings };
        this.recursiveSyncService.pushAndSyncToFrappe(
          url,
          headers,
          token,
          HttpMethod.GET,
          supportInfo,
          this.fetchRolesCallback.bind(this),
          this.setRolesFailure.bind(this),
          callbackParams,
        );
        return of({});
      }),
    );
  }

  fetchRolesCallback(
    success: { message: string[] },
    callbackParams: {
      supportInfo: any;
      token: TokenCache;
      email: string;
      serverSettings: ServerSettings;
    },
  ) {
    this.appendAndSyncRole(
      callbackParams.supportInfo,
      success.message,
      callbackParams.token,
      callbackParams.email,
      callbackParams.serverSettings,
    );
  }

  setRolesFailure(
    url: string,
    requestMethod: HttpMethod,
    frappeObject: any,
    callbackParams: any,
  ) {
    if (callbackParams.count > FRAPPE_FAILURE_RETRY_COUNT) {
      return;
    }
    callbackParams.count++;
    this.syncUserRoles(
      url,
      callbackParams.headers,
      callbackParams.token,
      callbackParams.supportInfo,
      callbackParams.rolesObject,
      callbackParams,
    );
  }

  appendAndSyncRole(
    supportInfo: { frappeConnectorURL: string; frappeServerUuid: string },
    roles: string[],
    token: TokenCache,
    email: string,
    settings: ServerSettings,
  ) {
    const url =
      supportInfo.frappeConnectorURL +
      FRAPPE_CONNECTOR_TRUSTED_CLIENT_ENDPOINT +
      supportInfo.frappeServerUuid +
      `/${settings.frappeSystemManagerUuid}` +
      ERP_SET_ROLE_ENDPOINT;

    const headers: any = {};
    headers[AUTHORIZATION] = BEARER + token.accessToken;
    headers[CONTENT_TYPE] = APPLICATION_JSON_HEADER;
    headers[ACCEPT] = APPLICATION_JSON_HEADER;
    roles.push(ERP_SYSTEM_MANAGER);
    const rolesObject: { uid: string; roles: string[] } = {
      uid: email,
      roles,
    };
    const callbackParams = {
      url,
      headers,
      token,
      method: HttpMethod.POST,
      supportInfo,
      rolesObject,
      count: 0,
    };

    this.syncUserRoles(
      url,
      headers,
      token,
      supportInfo,
      rolesObject,
      callbackParams,
    );
  }

  syncUserRoles(
    url: string,
    headers: OutgoingHttpHeaders,
    token: TokenCache,
    supportInfo: { frappeConnectorURL: string; frappeServerUuid: string },
    rolesObject: { uid: string; roles: string[] },
    callbackParams,
  ) {
    this.recursiveSyncService.pushAndSyncToFrappe(
      url,
      headers,
      token,
      HttpMethod.POST,
      supportInfo,
      null,
      this.setRolesFailure.bind(this),
      callbackParams,
      rolesObject,
    );
  }
}
