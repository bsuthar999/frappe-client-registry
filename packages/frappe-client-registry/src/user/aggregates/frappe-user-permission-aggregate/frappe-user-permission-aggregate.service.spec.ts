import { Test, TestingModule } from '@nestjs/testing';
import { FrappeUserPermissionAggregateService } from './frappe-user-permission-aggregate.service';
import { HttpService } from '@nestjs/common';
import { ServerSettingsService } from '../../../system-settings/entities/server-settings/server-settings.service';
import { ClientTokenManagerService } from '../../../auth/aggregates/client-token-manager/client-token-manager.service';
import { RecursiveSyncService } from '../../../frappe-sync/aggregates/recursive-sync/recursive-sync.service';

describe('FrappeUserPermissionAggregateService', () => {
  let service: FrappeUserPermissionAggregateService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        FrappeUserPermissionAggregateService,
        { provide: HttpService, useValue: {} },
        { provide: ServerSettingsService, useValue: {} },
        { provide: ClientTokenManagerService, useValue: {} },
        { provide: RecursiveSyncService, useValue: {} },
      ],
    }).compile();

    service = module.get<FrappeUserPermissionAggregateService>(
      FrappeUserPermissionAggregateService,
    );
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
