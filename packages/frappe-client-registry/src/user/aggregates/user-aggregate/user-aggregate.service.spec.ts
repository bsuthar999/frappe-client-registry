import { Test, TestingModule } from '@nestjs/testing';
import { UserAggregateService } from './user-aggregate.service';
import { UserService } from '../../entities/user/user.service';
import { ClientRoleService } from '../../../client-role/entity/client-role/client-role.service';
import { UserPoliciesService } from '../../policies/user-policies/user-policies.service';
import { ClientService } from '../../../client/entities/client/client.service';
import { ServerSettingsService } from '../../../system-settings/entities/server-settings/server-settings.service';
import { FrappeUserPermissionAggregateService } from '../frappe-user-permission-aggregate/frappe-user-permission-aggregate.service';

describe('UserAggregateService', () => {
  let service: UserAggregateService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        UserAggregateService,
        {
          provide: UserService,
          useValue: {},
        },
        {
          provide: ClientRoleService,
          useValue: {},
        },
        {
          provide: UserPoliciesService,
          useValue: {},
        },
        {
          provide: ClientService,
          useValue: {},
        },
        {
          provide: ServerSettingsService,
          useValue: {},
        },
        {
          provide: FrappeUserPermissionAggregateService,
          useValue: {},
        },
      ],
    }).compile();

    service = module.get<UserAggregateService>(UserAggregateService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
