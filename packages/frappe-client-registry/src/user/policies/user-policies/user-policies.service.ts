import { Injectable, BadRequestException } from '@nestjs/common';
import { from, throwError, of } from 'rxjs';
import { switchMap } from 'rxjs/operators';
import {
  USER_DOES_NOT_EXIST,
  USER_NOT_FOUND,
} from '../../../constants/messages';
import { UserService } from '../../entities/user/user.service';

@Injectable()
export class UserPoliciesService {
  constructor(private readonly userService: UserService) {}

  validateUser(userPayload) {
    return from(this.userService.findOne({ email: userPayload.email })).pipe(
      switchMap(user => {
        if (!user) {
          return throwError(new BadRequestException(USER_NOT_FOUND));
        }
        return of(user);
      }),
    );
  }

  validateExistingUser(updatePayload) {
    return from(this.userService.findOne({ email: updatePayload.email })).pipe(
      switchMap(user => {
        if (user) {
          return of(user);
        }
        return throwError(new BadRequestException(USER_DOES_NOT_EXIST));
      }),
    );
  }
}
