import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { User } from './user.entity';
import { MongoRepository } from 'typeorm';

@Injectable()
export class UserService {
  constructor(
    @InjectRepository(User)
    private readonly userRepository: MongoRepository<User>,
  ) {}

  async create(user: User) {
    const userObject = new User();
    Object.assign(userObject, user);
    return await userObject.save();
  }

  async findOne(param, option?) {
    return await this.userRepository.findOne(param, option);
  }

  async deleteOne(query, options?) {
    return await this.userRepository.deleteOne(query, options);
  }

  async updateOne(query, options?) {
    return await this.userRepository.updateOne(query, options);
  }

  async list(skip, take, search, sort) {
    const nameExp = new RegExp(search, 'i');
    const columns = this.userRepository.manager.connection
      .getMetadata(User)
      .ownColumns.map(column => column.propertyName);

    const $or = columns.map(field => {
      const filter = {};
      filter[field] = nameExp;
      return filter;
    });
    const $and: any[] = [{ $or }];

    const where: { $and: any } = { $and };

    const results = await this.userRepository.find({
      skip,
      take,
      where,
    });

    return {
      docs: results || [],
      length: await this.userRepository.count(where),
      offset: skip,
    };
  }

  async listByFilter(clientRoleUsers: string[], skip, take, search, sort) {
    const nameExp = new RegExp(search, 'i');
    const columns = this.userRepository.manager.connection
      .getMetadata(User)
      .ownColumns.map(column => column.propertyName);

    const $or = columns.map(field => {
      const filter = {};
      filter[field] = nameExp;
      return filter;
    });
    const $and: any[] = [{ $or }, { email: { $nin: clientRoleUsers } }];

    const where: { $and: any } = { $and };

    const results = await this.userRepository.find({
      skip,
      take,
      where,
    });

    return {
      docs: results || [],
      length: await this.userRepository.count(where),
      offset: skip,
    };
  }

  async count(query) {
    return this.userRepository.count(query);
  }
}
