import { Entity, BaseEntity, ObjectIdColumn, ObjectID, Column } from 'typeorm';

@Entity()
export class User extends BaseEntity {
  @ObjectIdColumn()
  _id: ObjectID;

  @Column()
  uuid: string;

  @Column()
  name: string;

  @Column()
  socialLogin: string;

  @Column()
  email: string;
}
