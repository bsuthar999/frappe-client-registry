import { Module, Global } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { CqrsModule } from '@nestjs/cqrs';
import { User } from './user/user.entity';
import { UserService } from './user/user.service';

@Global()
@Module({
  imports: [TypeOrmModule.forFeature([User]), CqrsModule],
  providers: [UserService],
  exports: [UserService],
})
export class UserEntitiesModule {}
