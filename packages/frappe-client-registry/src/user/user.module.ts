import { Module, HttpModule, Global } from '@nestjs/common';
import { UserController } from './controllers/user/user.controller';
import { UserEntitiesModule } from './entities/entity.module';
import { CqrsModule } from '@nestjs/cqrs';
import { UserAggregateManager } from './aggregates';
import { CommandManager } from './commands';
import { EventManager } from './events';
import { QueryManager } from './query';
import { UserPoliciesService } from './policies/user-policies/user-policies.service';

@Global()
@Module({
  controllers: [UserController],
  imports: [UserEntitiesModule, CqrsModule, HttpModule],
  providers: [
    ...UserAggregateManager,
    ...CommandManager,
    ...EventManager,
    ...QueryManager,
    UserPoliciesService,
  ],
  exports: [UserEntitiesModule, ...UserAggregateManager],
})
export class UserModule {}
