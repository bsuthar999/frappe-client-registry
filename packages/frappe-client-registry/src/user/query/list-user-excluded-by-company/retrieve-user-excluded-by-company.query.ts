import { IQuery } from '@nestjs/cqrs';

export class RetrieveUsersNotInCompanyQuery implements IQuery {
  constructor(
    public company: string,
    public offset: number,
    public limit: number,
    public search: string,
    public sort: string,
  ) {}
}
