import { IQuery } from '@nestjs/cqrs';

export class RetrieveUserQuery implements IQuery {
  constructor(public readonly uuid: string, public readonly req: any) {}
}
