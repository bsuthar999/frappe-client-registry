import { IQueryHandler, QueryHandler } from '@nestjs/cqrs';
import { RetrieveUserQuery } from './retrieve-user.query';
import { UserAggregateService } from '../../aggregates/user-aggregate/user-aggregate.service';

@QueryHandler(RetrieveUserQuery)
export class RetrieveUserHandler implements IQueryHandler<RetrieveUserQuery> {
  constructor(private readonly manager: UserAggregateService) {}

  async execute(query: RetrieveUserQuery) {
    const { req, uuid } = query;
    return this.manager.retrieveUser(uuid, req);
  }
}
