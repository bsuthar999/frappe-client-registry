import { IQueryHandler, QueryHandler } from '@nestjs/cqrs';
import { RetrieveUserListQuery } from './retrieve-user-list.query';
import { UserAggregateService } from '../../aggregates/user-aggregate/user-aggregate.service';

@QueryHandler(RetrieveUserListQuery)
export class RetrieveUserListHandler
  implements IQueryHandler<RetrieveUserListQuery> {
  constructor(private readonly manager: UserAggregateService) {}
  async execute(query: RetrieveUserListQuery) {
    const { offset, limit, search, sort } = query;
    return await this.manager.getUserList(
      Number(offset),
      Number(limit),
      search,
      sort,
    );
  }
}
