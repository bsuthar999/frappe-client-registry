import {
  Controller,
  Req,
  UseGuards,
  Get,
  Param,
  Query,
  UseInterceptors,
  HttpException,
} from '@nestjs/common';
import { QueryBus } from '@nestjs/cqrs';
import { TokenGuard } from '../../../auth/guards/token.guard';
import { RetrieveUserQuery } from '../../query/get-user/retrieve-user.query';
import { RetrieveUserListQuery } from '../../query/list-user/retrieve-user-list.query';
import { RetrieveUsersNotInCompanyQuery } from '../../query/list-user-excluded-by-company/retrieve-user-excluded-by-company.query';
import { RavenInterceptor } from 'nest-raven';

@Controller('user')
@UseInterceptors(
  new RavenInterceptor({
    filters: [
      {
        type: HttpException,
        filter: (exception: HttpException) => 499 < exception.getStatus(),
      },
    ],
  }),
)
export class UserController {
  constructor(private readonly queryBus: QueryBus) {}

  @Get('v1/get/:uuid')
  @UseGuards(TokenGuard)
  get(@Param('uuid') uuid, @Req() req) {
    return this.queryBus.execute(new RetrieveUserQuery(uuid, req));
  }

  @Get('v1/list')
  @UseGuards(TokenGuard)
  listRole(
    @Query('start') offset = 0,
    @Query('limit') limit = 10,
    @Query('search') search = '',
    @Query('sort') sort,
  ) {
    if (sort !== 'ASC') {
      sort = 'DESC';
    }
    return this.queryBus.execute(
      new RetrieveUserListQuery(offset, limit, search, sort),
    );
  }

  @Get('v1/exclude_by_company')
  @UseGuards(TokenGuard)
  listExcludedUsers(
    @Query('company') company,
    @Query('start') offset = 0,
    @Query('limit') limit = 10,
    @Query('search') search = '',
    @Query('sort') sort,
  ) {
    if (sort !== 'ASC') {
      sort = 'DESC';
    }
    return this.queryBus.execute(
      new RetrieveUsersNotInCompanyQuery(company, offset, limit, search, sort),
    );
  }
}
