import { IEvent } from '@nestjs/cqrs';
import { User } from '../../entities/user/user.entity';

export class UserRemovedEvent implements IEvent {
  constructor(public user: User) {}
}
