import { EventsHandler, IEventHandler } from '@nestjs/cqrs';
import { UserRemovedEvent } from './user-removed.event';
import { UserService } from '../../entities/user/user.service';

@EventsHandler(UserRemovedEvent)
export class UserRemovedHandler implements IEventHandler<UserRemovedEvent> {
  constructor(private readonly userService: UserService) {}
  async handle(event: UserRemovedEvent) {
    const { user } = event;
    await this.userService.deleteOne({ uuid: user.uuid });
  }
}
