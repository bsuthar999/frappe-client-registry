import { EventsHandler, IEventHandler } from '@nestjs/cqrs';
import { UserUpdatedEvent } from './user-updated.event';
import { UserService } from '../../entities/user/user.service';

@EventsHandler(UserUpdatedEvent)
export class UserUpdatedHandler implements IEventHandler<UserUpdatedEvent> {
  constructor(private readonly userRegistry: UserService) {}

  async handle(event: UserUpdatedEvent) {
    const { updatePayload } = event;
    await this.userRegistry.updateOne(
      { uuid: updatePayload.uuid },
      { $set: updatePayload },
    );
  }
}
