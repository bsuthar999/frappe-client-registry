import { IEvent } from '@nestjs/cqrs';
import { User } from '../../entities/user/user.entity';

export class UserUpdatedEvent implements IEvent {
  constructor(public updatePayload: User) {}
}
