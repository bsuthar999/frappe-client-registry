import { AddNewUserHandler } from './add-new-user/add-new-user.handler';
import { UpdateUserHandler } from './update-user/update-user.handler';
import { RemoveUserHandler } from './remove-user/remove-user.handler';

export const CommandManager = [
  AddNewUserHandler,
  UpdateUserHandler,
  UpdateUserHandler,
  RemoveUserHandler,
];
