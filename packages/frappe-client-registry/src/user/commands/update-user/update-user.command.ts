import { ICommand } from '@nestjs/cqrs';
import { UpdateUserDto } from '../../entities/user/update-user.dto';

export class UpdateUserCommand implements ICommand {
  constructor(public updatePayload: UpdateUserDto) {}
}
